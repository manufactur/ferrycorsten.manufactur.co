# WordPress MySQL database migration
#
# Generated: Thursday 23. March 2017 21:44 UTC
# Hostname: localhost:3306
# Database: `wordpress_f8`
# --------------------------------------------------------

/*!40101 SET NAMES utf8 */;

SET sql_mode='NO_AUTO_VALUE_ON_ZERO';



#
# Delete any existing table `blingbling_commentmeta`
#

DROP TABLE IF EXISTS `blingbling_commentmeta`;


#
# Table structure of table `blingbling_commentmeta`
#

CREATE TABLE `blingbling_commentmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `comment_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `comment_id` (`comment_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


#
# Data contents of table `blingbling_commentmeta`
#

#
# End of data contents of table `blingbling_commentmeta`
# --------------------------------------------------------



#
# Delete any existing table `blingbling_comments`
#

DROP TABLE IF EXISTS `blingbling_comments`;


#
# Table structure of table `blingbling_comments`
#

CREATE TABLE `blingbling_comments` (
  `comment_ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `comment_post_ID` bigint(20) unsigned NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`comment_ID`),
  KEY `comment_post_ID` (`comment_post_ID`),
  KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  KEY `comment_date_gmt` (`comment_date_gmt`),
  KEY `comment_parent` (`comment_parent`),
  KEY `comment_author_email` (`comment_author_email`(10))
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


#
# Data contents of table `blingbling_comments`
#
INSERT INTO `blingbling_comments` ( `comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
(1, 1, 'A WordPress Commenter', 'wapuu@wordpress.example', 'https://wordpress.org/', '', '2017-03-13 15:36:33', '2017-03-13 15:36:33', 'Hi, this is a comment.\nTo get started with moderating, editing, and deleting comments, please visit the Comments screen in the dashboard.\nCommenter avatars come from <a href="https://gravatar.com">Gravatar</a>.', 0, 'post-trashed', '', '', 0, 0) ;

#
# End of data contents of table `blingbling_comments`
# --------------------------------------------------------



#
# Delete any existing table `blingbling_links`
#

DROP TABLE IF EXISTS `blingbling_links`;


#
# Table structure of table `blingbling_links`
#

CREATE TABLE `blingbling_links` (
  `link_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) unsigned NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`link_id`),
  KEY `link_visible` (`link_visible`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


#
# Data contents of table `blingbling_links`
#

#
# End of data contents of table `blingbling_links`
# --------------------------------------------------------



#
# Delete any existing table `blingbling_options`
#

DROP TABLE IF EXISTS `blingbling_options`;


#
# Table structure of table `blingbling_options`
#

CREATE TABLE `blingbling_options` (
  `option_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'yes',
  PRIMARY KEY (`option_id`),
  UNIQUE KEY `option_name` (`option_name`)
) ENGINE=InnoDB AUTO_INCREMENT=467 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


#
# Data contents of table `blingbling_options`
#
INSERT INTO `blingbling_options` ( `option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://localhost:8888', 'yes'),
(2, 'home', 'http://localhost:8888', 'yes'),
(3, 'blogname', 'Ferry Corsten', 'yes'),
(4, 'blogdescription', 'Just another WordPress site', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'nicholas@manufactur.co', 'yes'),
(7, 'start_of_week', '1', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '0', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'F j, Y', 'yes'),
(24, 'time_format', 'g:i a', 'yes'),
(25, 'links_updated_date_format', 'F j, Y g:i a', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%postname%/', 'yes'),
(29, 'rewrite_rules', 'a:86:{s:11:"^wp-json/?$";s:22:"index.php?rest_route=/";s:14:"^wp-json/(.*)?";s:33:"index.php?rest_route=/$matches[1]";s:47:"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$";s:52:"index.php?category_name=$matches[1]&feed=$matches[2]";s:42:"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$";s:52:"index.php?category_name=$matches[1]&feed=$matches[2]";s:23:"category/(.+?)/embed/?$";s:46:"index.php?category_name=$matches[1]&embed=true";s:35:"category/(.+?)/page/?([0-9]{1,})/?$";s:53:"index.php?category_name=$matches[1]&paged=$matches[2]";s:17:"category/(.+?)/?$";s:35:"index.php?category_name=$matches[1]";s:44:"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?tag=$matches[1]&feed=$matches[2]";s:39:"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?tag=$matches[1]&feed=$matches[2]";s:20:"tag/([^/]+)/embed/?$";s:36:"index.php?tag=$matches[1]&embed=true";s:32:"tag/([^/]+)/page/?([0-9]{1,})/?$";s:43:"index.php?tag=$matches[1]&paged=$matches[2]";s:14:"tag/([^/]+)/?$";s:25:"index.php?tag=$matches[1]";s:45:"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?post_format=$matches[1]&feed=$matches[2]";s:40:"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?post_format=$matches[1]&feed=$matches[2]";s:21:"type/([^/]+)/embed/?$";s:44:"index.php?post_format=$matches[1]&embed=true";s:33:"type/([^/]+)/page/?([0-9]{1,})/?$";s:51:"index.php?post_format=$matches[1]&paged=$matches[2]";s:15:"type/([^/]+)/?$";s:33:"index.php?post_format=$matches[1]";s:12:"robots\\.txt$";s:18:"index.php?robots=1";s:48:".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$";s:18:"index.php?feed=old";s:20:".*wp-app\\.php(/.*)?$";s:19:"index.php?error=403";s:18:".*wp-register.php$";s:23:"index.php?register=true";s:32:"feed/(feed|rdf|rss|rss2|atom)/?$";s:27:"index.php?&feed=$matches[1]";s:27:"(feed|rdf|rss|rss2|atom)/?$";s:27:"index.php?&feed=$matches[1]";s:8:"embed/?$";s:21:"index.php?&embed=true";s:20:"page/?([0-9]{1,})/?$";s:28:"index.php?&paged=$matches[1]";s:27:"comment-page-([0-9]{1,})/?$";s:38:"index.php?&page_id=5&cpage=$matches[1]";s:41:"comments/feed/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?&feed=$matches[1]&withcomments=1";s:36:"comments/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?&feed=$matches[1]&withcomments=1";s:17:"comments/embed/?$";s:21:"index.php?&embed=true";s:44:"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:40:"index.php?s=$matches[1]&feed=$matches[2]";s:39:"search/(.+)/(feed|rdf|rss|rss2|atom)/?$";s:40:"index.php?s=$matches[1]&feed=$matches[2]";s:20:"search/(.+)/embed/?$";s:34:"index.php?s=$matches[1]&embed=true";s:32:"search/(.+)/page/?([0-9]{1,})/?$";s:41:"index.php?s=$matches[1]&paged=$matches[2]";s:14:"search/(.+)/?$";s:23:"index.php?s=$matches[1]";s:47:"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?author_name=$matches[1]&feed=$matches[2]";s:42:"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?author_name=$matches[1]&feed=$matches[2]";s:23:"author/([^/]+)/embed/?$";s:44:"index.php?author_name=$matches[1]&embed=true";s:35:"author/([^/]+)/page/?([0-9]{1,})/?$";s:51:"index.php?author_name=$matches[1]&paged=$matches[2]";s:17:"author/([^/]+)/?$";s:33:"index.php?author_name=$matches[1]";s:69:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$";s:80:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]";s:64:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$";s:80:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]";s:45:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$";s:74:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true";s:57:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$";s:81:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]";s:39:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$";s:63:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]";s:56:"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$";s:64:"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]";s:51:"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$";s:64:"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]";s:32:"([0-9]{4})/([0-9]{1,2})/embed/?$";s:58:"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true";s:44:"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$";s:65:"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]";s:26:"([0-9]{4})/([0-9]{1,2})/?$";s:47:"index.php?year=$matches[1]&monthnum=$matches[2]";s:43:"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?year=$matches[1]&feed=$matches[2]";s:38:"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?year=$matches[1]&feed=$matches[2]";s:19:"([0-9]{4})/embed/?$";s:37:"index.php?year=$matches[1]&embed=true";s:31:"([0-9]{4})/page/?([0-9]{1,})/?$";s:44:"index.php?year=$matches[1]&paged=$matches[2]";s:13:"([0-9]{4})/?$";s:26:"index.php?year=$matches[1]";s:27:".?.+?/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:37:".?.+?/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:57:".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:33:".?.+?/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:16:"(.?.+?)/embed/?$";s:41:"index.php?pagename=$matches[1]&embed=true";s:20:"(.?.+?)/trackback/?$";s:35:"index.php?pagename=$matches[1]&tb=1";s:40:"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?pagename=$matches[1]&feed=$matches[2]";s:35:"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?pagename=$matches[1]&feed=$matches[2]";s:28:"(.?.+?)/page/?([0-9]{1,})/?$";s:48:"index.php?pagename=$matches[1]&paged=$matches[2]";s:35:"(.?.+?)/comment-page-([0-9]{1,})/?$";s:48:"index.php?pagename=$matches[1]&cpage=$matches[2]";s:24:"(.?.+?)(?:/([0-9]+))?/?$";s:47:"index.php?pagename=$matches[1]&page=$matches[2]";s:27:"[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:37:"[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:57:"[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:"[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:"[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:33:"[^/]+/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:16:"([^/]+)/embed/?$";s:37:"index.php?name=$matches[1]&embed=true";s:20:"([^/]+)/trackback/?$";s:31:"index.php?name=$matches[1]&tb=1";s:40:"([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?name=$matches[1]&feed=$matches[2]";s:35:"([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?name=$matches[1]&feed=$matches[2]";s:28:"([^/]+)/page/?([0-9]{1,})/?$";s:44:"index.php?name=$matches[1]&paged=$matches[2]";s:35:"([^/]+)/comment-page-([0-9]{1,})/?$";s:44:"index.php?name=$matches[1]&cpage=$matches[2]";s:24:"([^/]+)(?:/([0-9]+))?/?$";s:43:"index.php?name=$matches[1]&page=$matches[2]";s:16:"[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:26:"[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:46:"[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:41:"[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:41:"[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:22:"[^/]+/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";}', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:4:{i:0;s:34:"advanced-custom-fields-pro/acf.php";i:1;s:33:"duplicate-post/duplicate-post.php";i:2;s:33:"social-warfare/social-warfare.php";i:3;s:39:"wp-migrate-db-pro/wp-migrate-db-pro.php";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '0', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', '', 'no'),
(40, 'template', 'ferrycorsten', 'yes'),
(41, 'stylesheet', 'ferrycorsten', 'yes'),
(42, 'comment_whitelist', '1', 'yes'),
(43, 'blacklist_keys', '', 'no'),
(44, 'comment_registration', '0', 'yes'),
(45, 'html_type', 'text/html', 'yes'),
(46, 'use_trackback', '0', 'yes'),
(47, 'default_role', 'subscriber', 'yes'),
(48, 'db_version', '37965', 'yes'),
(49, 'uploads_use_yearmonth_folders', '1', 'yes'),
(51, 'blog_public', '0', 'yes'),
(52, 'default_link_category', '2', 'yes'),
(53, 'show_on_front', 'page', 'yes'),
(54, 'tag_base', '', 'yes'),
(55, 'show_avatars', '1', 'yes'),
(56, 'avatar_rating', 'G', 'yes'),
(58, 'thumbnail_size_w', '150', 'yes'),
(59, 'thumbnail_size_h', '150', 'yes'),
(60, 'thumbnail_crop', '1', 'yes'),
(61, 'medium_size_w', '300', 'yes'),
(62, 'medium_size_h', '300', 'yes'),
(63, 'avatar_default', 'mystery', 'yes'),
(64, 'large_size_w', '1024', 'yes'),
(65, 'large_size_h', '1024', 'yes'),
(66, 'image_default_link_type', 'none', 'yes'),
(67, 'image_default_size', '', 'yes'),
(68, 'image_default_align', '', 'yes'),
(69, 'close_comments_for_old_posts', '0', 'yes'),
(70, 'close_comments_days_old', '14', 'yes'),
(71, 'thread_comments', '1', 'yes'),
(72, 'thread_comments_depth', '5', 'yes'),
(73, 'page_comments', '0', 'yes'),
(74, 'comments_per_page', '50', 'yes'),
(75, 'default_comments_page', 'newest', 'yes'),
(76, 'comment_order', 'asc', 'yes'),
(77, 'sticky_posts', 'a:0:{}', 'yes'),
(78, 'widget_categories', 'a:2:{i:2;a:4:{s:5:"title";s:0:"";s:5:"count";i:0;s:12:"hierarchical";i:0;s:8:"dropdown";i:0;}s:12:"_multiwidget";i:1;}', 'yes'),
(79, 'widget_text', 'a:0:{}', 'yes'),
(80, 'widget_rss', 'a:0:{}', 'yes'),
(81, 'uninstall_plugins', 'a:0:{}', 'no'),
(82, 'timezone_string', '', 'yes'),
(83, 'page_for_posts', '0', 'yes'),
(84, 'page_on_front', '5', 'yes'),
(85, 'default_post_format', '0', 'yes'),
(86, 'link_manager_enabled', '0', 'yes'),
(87, 'finished_splitting_shared_terms', '1', 'yes'),
(88, 'site_icon', '0', 'yes'),
(89, 'medium_large_size_w', '768', 'yes'),
(90, 'medium_large_size_h', '0', 'yes'),
(91, 'initial_db_version', '37965', 'yes'),
(92, 'blingbling_user_roles', 'a:5:{s:13:"administrator";a:2:{s:4:"name";s:13:"Administrator";s:12:"capabilities";a:62:{s:13:"switch_themes";b:1;s:11:"edit_themes";b:1;s:16:"activate_plugins";b:1;s:12:"edit_plugins";b:1;s:10:"edit_users";b:1;s:10:"edit_files";b:1;s:14:"manage_options";b:1;s:17:"moderate_comments";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:12:"upload_files";b:1;s:6:"import";b:1;s:15:"unfiltered_html";b:1;s:10:"edit_posts";b:1;s:17:"edit_others_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:10:"edit_pages";b:1;s:4:"read";b:1;s:8:"level_10";b:1;s:7:"level_9";b:1;s:7:"level_8";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:17:"edit_others_pages";b:1;s:20:"edit_published_pages";b:1;s:13:"publish_pages";b:1;s:12:"delete_pages";b:1;s:19:"delete_others_pages";b:1;s:22:"delete_published_pages";b:1;s:12:"delete_posts";b:1;s:19:"delete_others_posts";b:1;s:22:"delete_published_posts";b:1;s:20:"delete_private_posts";b:1;s:18:"edit_private_posts";b:1;s:18:"read_private_posts";b:1;s:20:"delete_private_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"read_private_pages";b:1;s:12:"delete_users";b:1;s:12:"create_users";b:1;s:17:"unfiltered_upload";b:1;s:14:"edit_dashboard";b:1;s:14:"update_plugins";b:1;s:14:"delete_plugins";b:1;s:15:"install_plugins";b:1;s:13:"update_themes";b:1;s:14:"install_themes";b:1;s:11:"update_core";b:1;s:10:"list_users";b:1;s:12:"remove_users";b:1;s:13:"promote_users";b:1;s:18:"edit_theme_options";b:1;s:13:"delete_themes";b:1;s:6:"export";b:1;s:10:"copy_posts";b:1;}}s:6:"editor";a:2:{s:4:"name";s:6:"Editor";s:12:"capabilities";a:35:{s:17:"moderate_comments";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:12:"upload_files";b:1;s:15:"unfiltered_html";b:1;s:10:"edit_posts";b:1;s:17:"edit_others_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:10:"edit_pages";b:1;s:4:"read";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:17:"edit_others_pages";b:1;s:20:"edit_published_pages";b:1;s:13:"publish_pages";b:1;s:12:"delete_pages";b:1;s:19:"delete_others_pages";b:1;s:22:"delete_published_pages";b:1;s:12:"delete_posts";b:1;s:19:"delete_others_posts";b:1;s:22:"delete_published_posts";b:1;s:20:"delete_private_posts";b:1;s:18:"edit_private_posts";b:1;s:18:"read_private_posts";b:1;s:20:"delete_private_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"read_private_pages";b:1;s:10:"copy_posts";b:1;}}s:6:"author";a:2:{s:4:"name";s:6:"Author";s:12:"capabilities";a:10:{s:12:"upload_files";b:1;s:10:"edit_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:4:"read";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:12:"delete_posts";b:1;s:22:"delete_published_posts";b:1;}}s:11:"contributor";a:2:{s:4:"name";s:11:"Contributor";s:12:"capabilities";a:5:{s:10:"edit_posts";b:1;s:4:"read";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:12:"delete_posts";b:1;}}s:10:"subscriber";a:2:{s:4:"name";s:10:"Subscriber";s:12:"capabilities";a:2:{s:4:"read";b:1;s:7:"level_0";b:1;}}}', 'yes'),
(93, 'widget_search', 'a:2:{i:2;a:1:{s:5:"title";s:0:"";}s:12:"_multiwidget";i:1;}', 'yes'),
(94, 'widget_recent-posts', 'a:2:{i:2;a:2:{s:5:"title";s:0:"";s:6:"number";i:5;}s:12:"_multiwidget";i:1;}', 'yes'),
(95, 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:"title";s:0:"";s:6:"number";i:5;}s:12:"_multiwidget";i:1;}', 'yes'),
(96, 'widget_archives', 'a:2:{i:2;a:3:{s:5:"title";s:0:"";s:5:"count";i:0;s:8:"dropdown";i:0;}s:12:"_multiwidget";i:1;}', 'yes'),
(97, 'widget_meta', 'a:2:{i:2;a:1:{s:5:"title";s:0:"";}s:12:"_multiwidget";i:1;}', 'yes'),
(98, 'sidebars_widgets', 'a:3:{s:19:"wp_inactive_widgets";a:0:{}s:18:"orphaned_widgets_1";a:6:{i:0;s:8:"search-2";i:1;s:14:"recent-posts-2";i:2;s:17:"recent-comments-2";i:3;s:10:"archives-2";i:4;s:12:"categories-2";i:5;s:6:"meta-2";}s:13:"array_version";i:3;}', 'yes'),
(99, 'widget_pages', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(100, 'widget_calendar', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(101, 'widget_tag_cloud', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(102, 'widget_nav_menu', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes') ;
INSERT INTO `blingbling_options` ( `option_id`, `option_name`, `option_value`, `autoload`) VALUES
(103, 'cron', 'a:4:{i:1490326593;a:3:{s:16:"wp_version_check";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}s:17:"wp_update_plugins";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}s:16:"wp_update_themes";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}}i:1490369952;a:1:{s:19:"wp_scheduled_delete";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}i:1490370043;a:1:{s:30:"wp_scheduled_auto_draft_delete";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}s:7:"version";i:2;}', 'yes'),
(135, 'recently_activated', 'a:0:{}', 'yes'),
(143, 'theme_mods_twentysixteen', 'a:1:{s:16:"sidebars_widgets";a:2:{s:4:"time";i:1489419657;s:4:"data";a:2:{s:19:"wp_inactive_widgets";a:0:{}s:9:"sidebar-1";a:6:{i:0;s:8:"search-2";i:1;s:14:"recent-posts-2";i:2;s:17:"recent-comments-2";i:3;s:10:"archives-2";i:4;s:12:"categories-2";i:5;s:6:"meta-2";}}}}', 'yes'),
(144, 'current_theme', 'Ferry Corsten', 'yes'),
(145, 'theme_mods_ferrycorsten', 'a:2:{i:0;b:0;s:18:"custom_css_post_id";i:-1;}', 'yes'),
(146, 'theme_switched', '', 'yes'),
(219, 'acf_version', '5.3.7', 'yes'),
(228, 'options_socials_0_network', 'facebook', 'no'),
(229, '_options_socials_0_network', 'field_58cfe54565a84', 'no'),
(230, 'options_socials_0_link', 'http://facebook.com/', 'no'),
(231, '_options_socials_0_link', 'field_58cfe56765a85', 'no'),
(232, 'options_socials_1_network', 'instagram', 'no'),
(233, '_options_socials_1_network', 'field_58cfe54565a84', 'no'),
(234, 'options_socials_1_link', 'http://instagram.com', 'no'),
(235, '_options_socials_1_link', 'field_58cfe56765a85', 'no'),
(236, 'options_socials_2_network', 'twitter', 'no'),
(237, '_options_socials_2_network', 'field_58cfe54565a84', 'no'),
(238, 'options_socials_2_link', 'http://twitter.com', 'no'),
(239, '_options_socials_2_link', 'field_58cfe56765a85', 'no'),
(240, 'options_socials', '3', 'no'),
(241, '_options_socials', 'field_58cfe52265a81', 'no'),
(244, 'duplicate_post_copytitle', '1', 'yes'),
(245, 'duplicate_post_copydate', '0', 'yes'),
(246, 'duplicate_post_copystatus', '0', 'yes'),
(247, 'duplicate_post_copyslug', '1', 'yes'),
(248, 'duplicate_post_copyexcerpt', '1', 'yes'),
(249, 'duplicate_post_copycontent', '1', 'yes'),
(250, 'duplicate_post_copythumbnail', '1', 'yes'),
(251, 'duplicate_post_copytemplate', '1', 'yes'),
(252, 'duplicate_post_copyformat', '1', 'yes'),
(253, 'duplicate_post_copyauthor', '0', 'yes'),
(254, 'duplicate_post_copypassword', '0', 'yes'),
(255, 'duplicate_post_copyattachments', '0', 'yes'),
(256, 'duplicate_post_copychildren', '0', 'yes'),
(257, 'duplicate_post_copycomments', '0', 'yes'),
(258, 'duplicate_post_copymenuorder', '1', 'yes'),
(259, 'duplicate_post_taxonomies_blacklist', 'a:0:{}', 'yes'),
(260, 'duplicate_post_blacklist', '', 'yes'),
(261, 'duplicate_post_types_enabled', 'a:2:{i:0;s:4:"post";i:1;s:4:"page";}', 'yes'),
(262, 'duplicate_post_show_row', '1', 'yes'),
(263, 'duplicate_post_show_adminbar', '1', 'yes'),
(264, 'duplicate_post_show_submitbox', '1', 'yes'),
(265, 'duplicate_post_show_bulkactions', '1', 'yes'),
(266, 'duplicate_post_version', '3.1.2', 'no'),
(267, 'duplicate_post_show_notice', '0', 'no'),
(272, 'options_header_image', '43', 'no'),
(273, '_options_header_image', 'field_58cfe86da407d', 'no'),
(325, 'widget_swp_popular_posts_widget', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(326, 'socialWarfareOptions', 'a:70:{s:12:"locationSite";s:4:"none";s:12:"locationHome";s:4:"none";s:13:"location_post";s:4:"none";s:13:"location_page";s:4:"none";s:5:"totes";b:1;s:9:"totesEach";b:1;s:9:"twitterID";s:12:"ferrycorsten";s:16:"swp_twitter_card";b:1;s:11:"visualTheme";s:9:"flatFresh";s:9:"dColorSet";s:9:"fullColor";s:9:"iColorSet";s:9:"fullColor";s:9:"oColorSet";s:9:"fullColor";s:13:"sideDColorSet";s:9:"fullColor";s:13:"sideIColorSet";s:9:"fullColor";s:13:"sideOColorSet";s:9:"fullColor";s:16:"floatStyleSource";b:1;s:10:"buttonSize";i:1;s:11:"buttonFloat";s:9:"fullWidth";s:10:"sideReveal";s:5:"slide";s:16:"swp_float_scr_sz";s:4:"1100";s:8:"cttTheme";s:6:"style1";s:14:"twitter_shares";b:0;s:5:"float";b:1;s:11:"floatOption";s:6:"bottom";s:12:"floatBgColor";s:7:"#ffffff";s:10:"floatStyle";s:7:"default";s:11:"customColor";s:7:"#000000";s:14:"recover_shares";b:0;s:15:"recovery_format";s:9:"unchanged";s:17:"recovery_protocol";s:9:"unchanged";s:15:"recovery_prefix";s:9:"unchanged";s:10:"swDecimals";s:1:"0";s:21:"swp_decimal_separator";s:6:"period";s:13:"swTotesFormat";s:8:"totesAlt";s:15:"googleAnalytics";b:0;s:15:"dashboardShares";b:1;s:14:"linkShortening";b:0;s:8:"minTotes";i:0;s:11:"cacheMethod";s:8:"advanced";s:12:"full_content";b:0;s:10:"rawNumbers";b:0;s:10:"notShowing";b:0;s:15:"visualEditorBug";b:0;s:7:"loopFix";b:0;s:12:"sniplyBuster";b:0;s:15:"analyticsMedium";s:6:"social";s:17:"analyticsCampaign";s:13:"SocialWarfare";s:18:"swp_click_tracking";b:0;s:18:"orderOfIconsSelect";s:6:"manual";s:12:"pinit_toggle";b:0;s:25:"pinit_location_horizontal";s:6:"center";s:23:"pinit_location_vertical";s:3:"top";s:15:"pinit_min_width";s:3:"200";s:16:"pinit_min_height";s:3:"200";s:15:"emphasize_icons";i:0;s:15:"floatLeftMobile";s:6:"bottom";s:15:"newOrderOfIcons";a:1:{s:7:"twitter";s:7:"twitter";}s:15:"sideCustomColor";s:7:"#000000";s:18:"locationattachment";s:4:"none";s:16:"locationrevision";s:4:"none";s:13:"nav_menu_item";s:4:"none";s:10:"shop_order";s:4:"none";s:17:"shop_order_refund";s:4:"none";s:11:"shop_coupon";s:4:"none";s:12:"shop_webhook";s:4:"none";s:19:"float_location_page";s:2:"on";s:19:"float_location_post";s:2:"on";s:11:"pinterestID";s:0:"";s:20:"facebookPublisherUrl";s:12:"FerryCorsten";s:13:"facebookAppID";s:0:"";}', 'yes'),
(375, 'options_spotify_follow_uri', 'spotify:artist:2ohlvFf9PBsDELdRstPtlP', 'no'),
(376, '_options_spotify_follow_uri', 'field_58d3cde1d4df3', 'no'),
(407, 'db_upgraded', '', 'yes'),
(444, 'upload_path', '', 'yes'),
(445, 'upload_url_path', '', 'yes'),
(446, 'wpmdb_schema_version', '1', 'no'),
(447, 'wpmdb_settings', 'a:11:{s:3:"key";s:40:"o41z8uceyjMdWg6BZKQKXClxov8CGoQHOkkcC3Ab";s:10:"allow_pull";b:0;s:10:"allow_push";b:0;s:8:"profiles";a:1:{i:0;a:19:{s:13:"save_computer";s:1:"1";s:9:"gzip_file";s:1:"1";s:13:"replace_guids";s:1:"1";s:12:"exclude_spam";s:1:"0";s:19:"keep_active_plugins";s:1:"0";s:13:"create_backup";s:1:"1";s:18:"exclude_post_types";s:1:"0";s:18:"exclude_transients";s:1:"1";s:25:"compatibility_older_mysql";s:1:"1";s:6:"action";s:4:"pull";s:15:"connection_info";s:76:"https://ferrycorsten.manufactur.co\r\nQJFqegtXBsJhhGdoFd67Jx4M0MfACVNSPj81QbOH";s:11:"replace_old";a:2:{i:1;s:28:"//ferrycorsten.manufactur.co";i:2;s:69:"/var/www/vhosts/fggs-mvlh.accessdomain.com/ferrycorsten.manufactur.co";}s:11:"replace_new";a:2:{i:1;s:16:"//localhost:8888";i:2;s:73:"/Users/chameleon_xing/Work/Manufactur/DEV Root/ferrycorsten.manufactur.co";}s:20:"table_migrate_option";s:24:"migrate_only_with_prefix";s:13:"backup_option";s:23:"backup_only_with_prefix";s:22:"save_migration_profile";s:1:"1";s:29:"save_migration_profile_option";s:3:"new";s:18:"create_new_profile";s:26:"ferrycorsten.manufactur.co";s:4:"name";s:26:"ferrycorsten.manufactur.co";}}s:7:"licence";s:36:"954a659e-2634-494f-947a-893d17f63321";s:10:"verify_ssl";b:0;s:17:"blacklist_plugins";a:0:{}s:11:"max_request";i:1048576;s:22:"delay_between_requests";i:0;s:18:"prog_tables_hidden";b:1;s:21:"pause_before_finalize";b:0;}', 'no'),
(450, '_site_transient_timeout_wpmdb_upgrade_data', '1490342104', 'no'),
(451, '_site_transient_wpmdb_upgrade_data', 'a:4:{s:17:"wp-migrate-db-pro";a:2:{s:7:"version";s:5:"1.7.1";s:6:"tested";s:5:"4.7.3";}s:29:"wp-migrate-db-pro-media-files";a:2:{s:7:"version";s:5:"1.4.7";s:6:"tested";s:5:"4.7.3";}s:21:"wp-migrate-db-pro-cli";a:2:{s:7:"version";s:3:"1.3";s:6:"tested";s:5:"4.7.3";}s:33:"wp-migrate-db-pro-multisite-tools";a:2:{s:7:"version";s:5:"1.1.5";s:6:"tested";s:5:"4.7.3";}}', 'no'),
(452, '_site_transient_update_core', 'O:8:"stdClass":4:{s:7:"updates";a:3:{i:0;O:8:"stdClass":10:{s:8:"response";s:7:"upgrade";s:8:"download";s:59:"https://downloads.wordpress.org/release/wordpress-4.7.3.zip";s:6:"locale";s:5:"en_US";s:8:"packages";O:8:"stdClass":5:{s:4:"full";s:59:"https://downloads.wordpress.org/release/wordpress-4.7.3.zip";s:10:"no_content";s:70:"https://downloads.wordpress.org/release/wordpress-4.7.3-no-content.zip";s:11:"new_bundled";s:71:"https://downloads.wordpress.org/release/wordpress-4.7.3-new-bundled.zip";s:7:"partial";b:0;s:8:"rollback";b:0;}s:7:"current";s:5:"4.7.3";s:7:"version";s:5:"4.7.3";s:11:"php_version";s:5:"5.2.4";s:13:"mysql_version";s:3:"5.0";s:11:"new_bundled";s:3:"4.7";s:15:"partial_version";s:0:"";}i:1;O:8:"stdClass":11:{s:8:"response";s:10:"autoupdate";s:8:"download";s:59:"https://downloads.wordpress.org/release/wordpress-4.7.3.zip";s:6:"locale";s:5:"en_US";s:8:"packages";O:8:"stdClass":5:{s:4:"full";s:59:"https://downloads.wordpress.org/release/wordpress-4.7.3.zip";s:10:"no_content";s:70:"https://downloads.wordpress.org/release/wordpress-4.7.3-no-content.zip";s:11:"new_bundled";s:71:"https://downloads.wordpress.org/release/wordpress-4.7.3-new-bundled.zip";s:7:"partial";b:0;s:8:"rollback";b:0;}s:7:"current";s:5:"4.7.3";s:7:"version";s:5:"4.7.3";s:11:"php_version";s:5:"5.2.4";s:13:"mysql_version";s:3:"5.0";s:11:"new_bundled";s:3:"4.7";s:15:"partial_version";s:0:"";s:9:"new_files";s:1:"1";}i:2;O:8:"stdClass":11:{s:8:"response";s:10:"autoupdate";s:8:"download";s:59:"https://downloads.wordpress.org/release/wordpress-4.6.4.zip";s:6:"locale";s:5:"en_US";s:8:"packages";O:8:"stdClass":5:{s:4:"full";s:59:"https://downloads.wordpress.org/release/wordpress-4.6.4.zip";s:10:"no_content";s:70:"https://downloads.wordpress.org/release/wordpress-4.6.4-no-content.zip";s:11:"new_bundled";s:71:"https://downloads.wordpress.org/release/wordpress-4.6.4-new-bundled.zip";s:7:"partial";s:69:"https://downloads.wordpress.org/release/wordpress-4.6.4-partial-1.zip";s:8:"rollback";s:70:"https://downloads.wordpress.org/release/wordpress-4.6.4-rollback-1.zip";}s:7:"current";s:5:"4.6.4";s:7:"version";s:5:"4.6.4";s:11:"php_version";s:5:"5.2.4";s:13:"mysql_version";s:3:"5.0";s:11:"new_bundled";s:3:"4.7";s:15:"partial_version";s:5:"4.6.1";s:9:"new_files";s:0:"";}}s:12:"last_checked";i:1490298905;s:15:"version_checked";s:5:"4.6.1";s:12:"translations";a:16:{i:0;a:7:{s:4:"type";s:4:"core";s:4:"slug";s:7:"default";s:8:"language";s:5:"da_DK";s:7:"version";s:5:"4.6.1";s:7:"updated";s:19:"2016-09-29 14:03:59";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.6.1/da_DK.zip";s:10:"autoupdate";b:1;}i:1;a:7:{s:4:"type";s:4:"core";s:4:"slug";s:7:"default";s:8:"language";s:5:"de_DE";s:7:"version";s:5:"4.6.1";s:7:"updated";s:19:"2016-12-21 21:20:26";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.6.1/de_DE.zip";s:10:"autoupdate";b:1;}i:2;a:7:{s:4:"type";s:4:"core";s:4:"slug";s:7:"default";s:8:"language";s:2:"el";s:7:"version";s:5:"4.6.1";s:7:"updated";s:19:"2016-11-09 20:42:31";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.6.1/el.zip";s:10:"autoupdate";b:1;}i:3;a:7:{s:4:"type";s:4:"core";s:4:"slug";s:7:"default";s:8:"language";s:5:"es_ES";s:7:"version";s:5:"4.6.1";s:7:"updated";s:19:"2016-10-31 08:03:58";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.6.1/es_ES.zip";s:10:"autoupdate";b:1;}i:4;a:7:{s:4:"type";s:4:"core";s:4:"slug";s:7:"default";s:8:"language";s:5:"fr_FR";s:7:"version";s:5:"4.6.1";s:7:"updated";s:19:"2016-11-02 11:49:52";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.6.1/fr_FR.zip";s:10:"autoupdate";b:1;}i:5;a:7:{s:4:"type";s:4:"core";s:4:"slug";s:7:"default";s:8:"language";s:5:"id_ID";s:7:"version";s:5:"4.6.1";s:7:"updated";s:19:"2016-09-22 05:34:53";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.6.1/id_ID.zip";s:10:"autoupdate";b:1;}i:6;a:7:{s:4:"type";s:4:"core";s:4:"slug";s:7:"default";s:8:"language";s:5:"it_IT";s:7:"version";s:5:"4.6.1";s:7:"updated";s:19:"2016-12-19 08:05:09";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.6.1/it_IT.zip";s:10:"autoupdate";b:1;}i:7;a:7:{s:4:"type";s:4:"core";s:4:"slug";s:7:"default";s:8:"language";s:2:"ja";s:7:"version";s:5:"4.6.1";s:7:"updated";s:19:"2016-11-01 15:23:06";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.6.1/ja.zip";s:10:"autoupdate";b:1;}i:8;a:7:{s:4:"type";s:4:"core";s:4:"slug";s:7:"default";s:8:"language";s:5:"ko_KR";s:7:"version";s:5:"4.6.1";s:7:"updated";s:19:"2016-09-24 07:18:31";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.6.1/ko_KR.zip";s:10:"autoupdate";b:1;}i:9;a:7:{s:4:"type";s:4:"core";s:4:"slug";s:7:"default";s:8:"language";s:5:"nl_NL";s:7:"version";s:5:"4.6.1";s:7:"updated";s:19:"2017-01-08 18:34:47";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.6.1/nl_NL.zip";s:10:"autoupdate";b:1;}i:10;a:7:{s:4:"type";s:4:"core";s:4:"slug";s:7:"default";s:8:"language";s:5:"pl_PL";s:7:"version";s:5:"4.6.1";s:7:"updated";s:19:"2016-09-22 09:54:16";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.6.1/pl_PL.zip";s:10:"autoupdate";b:1;}i:11;a:7:{s:4:"type";s:4:"core";s:4:"slug";s:7:"default";s:8:"language";s:5:"pt_BR";s:7:"version";s:5:"4.6.1";s:7:"updated";s:19:"2016-12-26 14:59:30";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.6.1/pt_BR.zip";s:10:"autoupdate";b:1;}i:12;a:7:{s:4:"type";s:4:"core";s:4:"slug";s:7:"default";s:8:"language";s:5:"pt_PT";s:7:"version";s:5:"4.6.1";s:7:"updated";s:19:"2017-01-10 08:18:16";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.6.1/pt_PT.zip";s:10:"autoupdate";b:1;}i:13;a:7:{s:4:"type";s:4:"core";s:4:"slug";s:7:"default";s:8:"language";s:5:"sv_SE";s:7:"version";s:5:"4.6.1";s:7:"updated";s:19:"2016-12-01 10:58:06";s:7:"package";s:64:"https://downloads.wordpress.org/translation/core/4.6.1/sv_SE.zip";s:10:"autoupdate";b:1;}i:14;a:7:{s:4:"type";s:4:"core";s:4:"slug";s:7:"default";s:8:"language";s:2:"th";s:7:"version";s:5:"4.6.1";s:7:"updated";s:19:"2016-10-12 07:04:13";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.6.1/th.zip";s:10:"autoupdate";b:1;}i:15;a:7:{s:4:"type";s:4:"core";s:4:"slug";s:7:"default";s:8:"language";s:2:"uk";s:7:"version";s:5:"4.6.1";s:7:"updated";s:19:"2017-01-04 23:08:07";s:7:"package";s:61:"https://downloads.wordpress.org/translation/core/4.6.1/uk.zip";s:10:"autoupdate";b:1;}}}', 'no'),
(453, '_transient_timeout_acf_pro_get_remote_info', '1490342105', 'no'),
(454, '_transient_acf_pro_get_remote_info', 'a:15:{s:4:"name";s:26:"Advanced Custom Fields PRO";s:4:"slug";s:26:"advanced-custom-fields-pro";s:8:"homepage";s:37:"https://www.advancedcustomfields.com/";s:7:"version";s:6:"5.5.10";s:6:"author";s:13:"Elliot Condon";s:10:"author_url";s:28:"http://www.elliotcondon.com/";s:12:"contributors";s:12:"elliotcondon";s:8:"requires";s:5:"3.6.0";s:6:"tested";s:5:"4.8.0";s:4:"tags";a:56:{i:0;s:5:"5.5.9";i:1;s:5:"5.5.7";i:2;s:5:"5.5.5";i:3;s:5:"5.5.3";i:4;s:5:"5.5.2";i:5;s:5:"5.5.1";i:6;s:5:"5.5.0";i:7;s:5:"5.4.8";i:8;s:5:"5.4.7";i:9;s:5:"5.4.6";i:10;s:5:"5.4.5";i:11;s:5:"5.4.4";i:12;s:5:"5.4.3";i:13;s:5:"5.4.2";i:14;s:5:"5.4.1";i:15;s:5:"5.4.0";i:16;s:5:"5.3.8";i:17;s:5:"5.3.7";i:18;s:5:"5.3.6";i:19;s:5:"5.3.5";i:20;s:5:"5.3.4";i:21;s:5:"5.3.3";i:22;s:5:"5.3.2";i:23;s:6:"5.3.10";i:24;s:5:"5.3.1";i:25;s:5:"5.3.0";i:26;s:5:"5.2.9";i:27;s:5:"5.2.8";i:28;s:5:"5.2.7";i:29;s:5:"5.2.6";i:30;s:5:"5.2.5";i:31;s:5:"5.2.4";i:32;s:5:"5.2.3";i:33;s:5:"5.2.2";i:34;s:5:"5.2.1";i:35;s:5:"5.2.0";i:36;s:5:"5.1.9";i:37;s:5:"5.1.8";i:38;s:5:"5.1.7";i:39;s:5:"5.1.6";i:40;s:5:"5.1.5";i:41;s:5:"5.1.4";i:42;s:5:"5.1.3";i:43;s:5:"5.1.2";i:44;s:5:"5.1.1";i:45;s:5:"5.1.0";i:46;s:5:"5.0.9";i:47;s:5:"5.0.8";i:48;s:5:"5.0.7";i:49;s:5:"5.0.6";i:50;s:5:"5.0.5";i:51;s:5:"5.0.4";i:52;s:5:"5.0.3";i:53;s:5:"5.0.2";i:54;s:5:"5.0.1";i:55;s:5:"5.0.0";}s:6:"tagged";s:123:"acf, advanced, custom, field, fields, custom field, custom fields, simple fields, magic fields, more fields, repeater, edit";s:11:"description";s:4337:"<p>Advanced Custom Fields is the perfect solution for any WordPress website which needs more flexible data like other Content Management Systems. </p>\n<ul><li>Visually create your Fields</li><li>Select from multiple input types (text, textarea, wysiwyg, image, file, page link, post object, relationship, select, checkbox, radio buttons, date picker, true / false, repeater, flexible content, gallery and more to come!)</li><li>Assign your fields to multiple edit pages (via custom location rules)</li><li>Easily load data through a simple and friendly API</li><li>Uses the native WordPress custom post type for ease of use and fast processing</li><li>Uses the native WordPress metadata for ease of use and fast processing</li></ul>\n<h4> Field Types </h4>\n<ul><li>Text (type text, api returns text)</li><li>Text Area (type text, api returns text)</li><li>Number (type number, api returns integer)</li><li>Email (type email, api returns text)</li><li>Password (type password, api returns text)</li><li>WYSIWYG (a WordPress wysiwyg editor, api returns html)</li><li>Image (upload an image, api returns the url)</li><li>File (upload a file, api returns the url)</li><li>Select (drop down list of choices, api returns chosen item)</li><li>Checkbox (tickbox list of choices, api returns array of choices)</li><li>Radio Buttons ( radio button list of choices, api returns chosen item)</li><li>True / False (tick box with message, api returns true or false)</li><li>Page Link (select 1 or more page, post or custom post types, api returns the selected url)</li><li>Post Object (select 1 or more page, post or custom post types, api returns the selected post objects)</li><li>Relationship (search, select and order post objects with a tidy interface, api returns the selected post objects)</li><li>Taxonomy (select taxonomy terms with options to load, display and save, api returns the selected term objects)</li><li>User (select 1 or more WP users, api returns the selected user objects)</li><li>Google Maps (interactive map, api returns lat,lng,address data)</li><li>Date Picker (jquery date picker, options for format, api returns string)</li><li>Color Picker (WP color swatch picker)</li><li>Tab (Group fields into tabs)</li><li>Message (Render custom messages into the fields)</li><li>Repeater (ability to create repeatable blocks of fields!)</li><li>Flexible Content (ability to create flexible blocks of fields!)</li><li>Gallery (Add, edit and order multiple images in 1 simple field)</li><li>[Custom](<a href="https://www.advancedcustomfields.com/resources/tutorials/creating-a-new-field-type/)">www.advancedcustomfields.com/resources/tutorials/creating-a-new-field-type/)</a> (Create your own field type!)</li></ul>\n<h4> Tested on </h4>\n<ul><li>Mac Firefox 	:)</li><li>Mac Safari 	:)</li><li>Mac Chrome	:)</li><li>PC Safari 	:)</li><li>PC Chrome		:)</li><li>PC Firefox	:)</li><li>iPhone Safari :)</li><li>iPad Safari 	:)</li><li>PC ie7		:S</li></ul>\n<h4> Website </h4>\n<p><a href="https://www.advancedcustomfields.com/">www.advancedcustomfields.com/</a></p>\n<h4> Documentation </h4>\n<ul><li>[Getting Started](<a href="https://www.advancedcustomfields.com/resources/#getting-started)">www.advancedcustomfields.com/resources/#getting-started)</a></li><li>[Field Types](<a href="https://www.advancedcustomfields.com/resources/#field-types)">www.advancedcustomfields.com/resources/#field-types)</a></li><li>[Functions](<a href="https://www.advancedcustomfields.com/resources/#functions)">www.advancedcustomfields.com/resources/#functions)</a></li><li>[Actions](<a href="https://www.advancedcustomfields.com/resources/#actions)">www.advancedcustomfields.com/resources/#actions)</a></li><li>[Filters](<a href="https://www.advancedcustomfields.com/resources/#filters)">www.advancedcustomfields.com/resources/#filters)</a></li><li>[How to guides](<a href="https://www.advancedcustomfields.com/resources/#how-to)">www.advancedcustomfields.com/resources/#how-to)</a></li><li>[Tutorials](<a href="https://www.advancedcustomfields.com/resources/#tutorials)">www.advancedcustomfields.com/resources/#tutorials)</a></li></ul>\n<h4> Bug Submission and Forum Support </h4>\n<p><a href="http://support.advancedcustomfields.com/">support.advancedcustomfields.com/</a></p>\n<h4> Please Vote and Enjoy </h4>\n<p>Your votes really make a difference! Thanks.</p>\n";s:12:"installation";s:467:"<ol><li>Upload <code>advanced-custom-fields</code> to the <code>/wp-content/plugins/</code> directory</li><li>Activate the plugin through the <code>Plugins</code> menu in WordPress</li><li>Click on the new menu item "Custom Fields" and create your first Custom Field Group!</li><li>Your custom field group will now appear on the page / post / template you specified in the field group\'s location rules!</li><li>Read the documentation to display your data: </li></ol>\n";s:9:"changelog";s:4928:"<h4> 5.5.10 </h4>\n<ul><li>API: Added new functionality to the `acf_form()` function:</li><li>- added new <code>html_updated_message</code> setting</li><li>- added new <code>html_submit_button</code> setting</li><li>- added new <code>html_submit_spinner</code> setting</li><li>- added new <code>acf/pre_submit_form</code> filter run when form is successfully submit (before saving $_POST)</li><li>- added new <code>acf/submit_form</code> action run when form is successfully submit (after saving $_POST)</li><li>- added new <code>%post_id%</code> replace string to the <code>return</code> setting</li><li>- added new encryption logic to prevent $_POST exploits</li><li>- added new `acf_register_form()` function</li><li>Core: Fixed bug preventing values being loaded on a new post/page preview</li><li>Core: Fixed missing <code>Bulk Actions</code> dropdown on sync screen when no field groups exist</li><li>Core: Fixed bug ignoring PHP field groups if exists in JSON</li><li>Core: Minor fixes and improvements</li></ul>\n<h4> 5.5.9 </h4>\n<ul><li>Core: Fixed bug causing ACF4 PHP field groups to be ignored if missing ‘key’ setting</li></ul>\n<h4> 5.5.8 </h4>\n<ul><li>Flexible Content: Added logic to better <code>clean up</code> data when re-ordering layouts</li><li>oEmbed field: Fixed bug causing incorrect width and height settings in embed HTML</li><li>Core: Fixed bug causing incorrect Select2 CSS version loading for WooCommerce 2.7</li><li>Core: Fixed bug preventing <code>min-height</code> style being applied to floating width fields</li><li>Core: Added new JS <code>init</code> actions for wysiwyg, date, datetime, time and select2 fields</li><li>Core: Minor fixes and improvements</li></ul>\n<h4> 5.5.7 </h4>\n<ul><li>Core: Fixed bug causing `get_field()` to return incorrect data for sub fields registered via PHP code.</li></ul>\n<h4> 5.5.6 </h4>\n<ul><li>Core: Fixed bug causing license key to be ignored after changing url from http to https</li><li>Core: Fixed Select2 (v4) bug where <code>allow null</code> setting would not correctly save empty value</li><li>Core: Added new <code>acf/validate_field</code> filter</li><li>Core: Added new <code>acf/validate_field_group</code> filter</li><li>Core: Added new <code>acf/validate_post_id</code> filter</li><li>Core: Added new <code>row_index_offset</code> setting</li><li>Core: Fixed bug causing value loading issues for a taxonomy term in WP < 4.4</li><li>Core: Minor fixes and improvements</li></ul>\n<h4> 5.5.5 </h4>\n<ul><li>File field: Fixed bug creating draft post when saving an empty value</li><li>Image field: Fixed bug mentioned above</li></ul>\n<h4> 5.5.4 </h4>\n<ul><li>File field: Added logic to <code>connect</code> selected attachment to post (only if attachment is not <code>connected</code>)</li><li>File field: Removed `filesize()` call causing performance issues with externally hosted attachments</li><li>File field: Added AJAX validation to <code>basic</code> uploader</li><li>Image field: Added <code>connect</code> logic mentioned above</li><li>Image field: Added AJAX validation mentioned above</li><li>True false field: Improved usability by allowing <code>tab</code> key to focus element (use space or arrow keys to toggle)</li><li>Gallery field: Fixed bug causing unsaved changes in sidebar to be lost when selecting another attachment</li><li>API: Fixed `add_row()` and `add_sub_row()` return values (from true to new row index)</li><li>Core: Improved `get_posts()` query speeds by setting <code>update_cache</code> settings to false</li><li>Core: Allowed <code>instruction_placement</code> setting on <code>widget</code> forms (previously set always to <code>below fields</code>)</li><li>Core: Removed <code>ACF PRO invalid license nag</code> and will include fix for <code>protocol change</code> in next release</li><li>Language: Updated French translation - thanks to Martial Parfait</li></ul>\n<h4> 5.5.3 </h4>\n<ul><li>Options page: Fixed bug when using WPML in multiple tabs causing incorrect <code>lang</code> to be used during save.</li><li>Core: Added support with new `get_user_locale()` setting in WP 4.7</li><li>Core: Improved efficiency of termmeta DB upgrade logic</li><li>Core: Minor fixes and improvements</li></ul>\n<h4> 5.5.2 </h4>\n<ul><li>Tab field: Fixed bug causing value loading issues for field\'s with the same name</li><li>Repeater field: Fixed bug in <code>collapsed</code> setting where field key was shown instead of field label</li></ul>\n<h4> 5.5.1 </h4>\n<ul><li>Select field: Fixed bug preventing some field settings from being selected</li><li>Date picker field: Improved compatibility with customised values</li><li>Core: Added new <code>enqueue_datepicker</code> setting which can be used to prevent the library from being enqueued</li><li>Core: Added new <code>enqueue_datetimepicker</code> setting which can be used to prevent the library from being enqueued</li><li>Core: Minor fixes and improvements</li></ul>\n";s:14:"upgrade_notice";s:551:"<h4> 5.2.7 </h4>\n<ul><li>Field class names have changed slightly in v5.2.7 from `field_type-{$type}` to `acf-field-{$type}`. This change was introduced to better optimise JS performance. The previous class names can be added back in with the following filter: <a href="https://www.advancedcustomfields.com/resources/acfcompatibility/">www.advancedcustomfields.com/resources/acfcompatibility/</a></li></ul>\n<h4> 3.0.0 </h4>\n<ul><li>Editor is broken in WordPress 3.3</li></ul>\n<h4> 2.1.4 </h4>\n<ul><li>Adds post_id column back into acf_values</li></ul>\n";}', 'no'),
(455, '_site_transient_update_plugins', 'O:8:"stdClass":5:{s:12:"last_checked";i:1490299409;s:7:"checked";a:12:{s:34:"advanced-custom-fields-pro/acf.php";s:5:"5.3.7";s:19:"akismet/akismet.php";s:3:"3.3";s:41:"birds-custom-login/birds-custom-login.php";s:5:"1.0.8";s:43:"custom-post-type-ui/custom-post-type-ui.php";s:5:"1.5.2";s:33:"duplicate-post/duplicate-post.php";s:5:"3.1.2";s:31:"featured-post/featured-post.php";s:5:"3.2.1";s:29:"gravityforms/gravityforms.php";s:6:"1.9.19";s:9:"hello.php";s:3:"1.6";s:41:"password-protected/password-protected.php";s:5:"2.0.3";s:47:"regenerate-thumbnails/regenerate-thumbnails.php";s:5:"2.2.6";s:33:"social-warfare/social-warfare.php";s:5:"2.2.3";s:39:"wp-migrate-db-pro/wp-migrate-db-pro.php";s:5:"1.7.1";}s:8:"response";a:1:{s:34:"advanced-custom-fields-pro/acf.php";O:8:"stdClass":5:{s:4:"slug";s:26:"advanced-custom-fields-pro";s:6:"plugin";s:34:"advanced-custom-fields-pro/acf.php";s:11:"new_version";s:6:"5.5.10";s:3:"url";s:37:"https://www.advancedcustomfields.com/";s:7:"package";s:0:"";}}s:12:"translations";a:61:{i:0;a:7:{s:4:"type";s:6:"plugin";s:4:"slug";s:7:"akismet";s:8:"language";s:5:"da_DK";s:7:"version";s:3:"3.1";s:7:"updated";s:19:"2015-04-13 17:42:38";s:7:"package";s:72:"https://downloads.wordpress.org/translation/plugin/akismet/3.1/da_DK.zip";s:10:"autoupdate";b:1;}i:1;a:7:{s:4:"type";s:6:"plugin";s:4:"slug";s:7:"akismet";s:8:"language";s:5:"de_DE";s:7:"version";s:3:"3.2";s:7:"updated";s:19:"2016-11-03 01:32:31";s:7:"package";s:72:"https://downloads.wordpress.org/translation/plugin/akismet/3.2/de_DE.zip";s:10:"autoupdate";b:1;}i:2;a:7:{s:4:"type";s:6:"plugin";s:4:"slug";s:7:"akismet";s:8:"language";s:2:"el";s:7:"version";s:6:"3.1.11";s:7:"updated";s:19:"2016-05-12 18:04:39";s:7:"package";s:72:"https://downloads.wordpress.org/translation/plugin/akismet/3.1.11/el.zip";s:10:"autoupdate";b:1;}i:3;a:7:{s:4:"type";s:6:"plugin";s:4:"slug";s:7:"akismet";s:8:"language";s:5:"es_ES";s:7:"version";s:3:"3.2";s:7:"updated";s:19:"2017-01-18 09:16:49";s:7:"package";s:72:"https://downloads.wordpress.org/translation/plugin/akismet/3.2/es_ES.zip";s:10:"autoupdate";b:1;}i:4;a:7:{s:4:"type";s:6:"plugin";s:4:"slug";s:7:"akismet";s:8:"language";s:2:"fi";s:7:"version";s:3:"3.2";s:7:"updated";s:19:"2016-12-06 06:15:43";s:7:"package";s:69:"https://downloads.wordpress.org/translation/plugin/akismet/3.2/fi.zip";s:10:"autoupdate";b:1;}i:5;a:7:{s:4:"type";s:6:"plugin";s:4:"slug";s:7:"akismet";s:8:"language";s:5:"fr_FR";s:7:"version";s:3:"3.2";s:7:"updated";s:19:"2016-11-12 10:32:54";s:7:"package";s:72:"https://downloads.wordpress.org/translation/plugin/akismet/3.2/fr_FR.zip";s:10:"autoupdate";b:1;}i:6;a:7:{s:4:"type";s:6:"plugin";s:4:"slug";s:7:"akismet";s:8:"language";s:5:"id_ID";s:7:"version";s:3:"3.1";s:7:"updated";s:19:"2015-04-21 23:53:01";s:7:"package";s:72:"https://downloads.wordpress.org/translation/plugin/akismet/3.1/id_ID.zip";s:10:"autoupdate";b:1;}i:7;a:7:{s:4:"type";s:6:"plugin";s:4:"slug";s:7:"akismet";s:8:"language";s:5:"it_IT";s:7:"version";s:3:"3.3";s:7:"updated";s:19:"2017-02-23 17:31:09";s:7:"package";s:72:"https://downloads.wordpress.org/translation/plugin/akismet/3.3/it_IT.zip";s:10:"autoupdate";b:1;}i:8;a:7:{s:4:"type";s:6:"plugin";s:4:"slug";s:7:"akismet";s:8:"language";s:2:"ja";s:7:"version";s:3:"3.3";s:7:"updated";s:19:"2017-03-06 20:08:59";s:7:"package";s:69:"https://downloads.wordpress.org/translation/plugin/akismet/3.3/ja.zip";s:10:"autoupdate";b:1;}i:9;a:7:{s:4:"type";s:6:"plugin";s:4:"slug";s:7:"akismet";s:8:"language";s:5:"ko_KR";s:7:"version";s:3:"3.1";s:7:"updated";s:19:"2015-04-22 14:17:52";s:7:"package";s:72:"https://downloads.wordpress.org/translation/plugin/akismet/3.1/ko_KR.zip";s:10:"autoupdate";b:1;}i:10;a:7:{s:4:"type";s:6:"plugin";s:4:"slug";s:7:"akismet";s:8:"language";s:5:"nb_NO";s:7:"version";s:5:"3.1.5";s:7:"updated";s:19:"2015-09-10 16:30:51";s:7:"package";s:74:"https://downloads.wordpress.org/translation/plugin/akismet/3.1.5/nb_NO.zip";s:10:"autoupdate";b:1;}i:11;a:7:{s:4:"type";s:6:"plugin";s:4:"slug";s:7:"akismet";s:8:"language";s:5:"nl_NL";s:7:"version";s:3:"3.2";s:7:"updated";s:19:"2016-09-13 08:53:28";s:7:"package";s:72:"https://downloads.wordpress.org/translation/plugin/akismet/3.2/nl_NL.zip";s:10:"autoupdate";b:1;}i:12;a:7:{s:4:"type";s:6:"plugin";s:4:"slug";s:7:"akismet";s:8:"language";s:5:"pl_PL";s:7:"version";s:6:"3.1.11";s:7:"updated";s:19:"2016-05-12 18:05:47";s:7:"package";s:75:"https://downloads.wordpress.org/translation/plugin/akismet/3.1.11/pl_PL.zip";s:10:"autoupdate";b:1;}i:13;a:7:{s:4:"type";s:6:"plugin";s:4:"slug";s:7:"akismet";s:8:"language";s:5:"pt_PT";s:7:"version";s:3:"3.3";s:7:"updated";s:19:"2017-02-23 17:31:27";s:7:"package";s:72:"https://downloads.wordpress.org/translation/plugin/akismet/3.3/pt_PT.zip";s:10:"autoupdate";b:1;}i:14;a:7:{s:4:"type";s:6:"plugin";s:4:"slug";s:7:"akismet";s:8:"language";s:5:"ru_RU";s:7:"version";s:3:"3.2";s:7:"updated";s:19:"2016-10-18 23:30:22";s:7:"package";s:72:"https://downloads.wordpress.org/translation/plugin/akismet/3.2/ru_RU.zip";s:10:"autoupdate";b:1;}i:15;a:7:{s:4:"type";s:6:"plugin";s:4:"slug";s:7:"akismet";s:8:"language";s:5:"sv_SE";s:7:"version";s:3:"3.3";s:7:"updated";s:19:"2017-02-23 17:31:40";s:7:"package";s:72:"https://downloads.wordpress.org/translation/plugin/akismet/3.3/sv_SE.zip";s:10:"autoupdate";b:1;}i:16;a:7:{s:4:"type";s:6:"plugin";s:4:"slug";s:7:"akismet";s:8:"language";s:2:"th";s:7:"version";s:6:"3.1.11";s:7:"updated";s:19:"2016-05-12 18:06:30";s:7:"package";s:72:"https://downloads.wordpress.org/translation/plugin/akismet/3.1.11/th.zip";s:10:"autoupdate";b:1;}i:17;a:7:{s:4:"type";s:6:"plugin";s:4:"slug";s:7:"akismet";s:8:"language";s:2:"tl";s:7:"version";s:6:"3.1.11";s:7:"updated";s:19:"2016-05-12 18:06:24";s:7:"package";s:72:"https://downloads.wordpress.org/translation/plugin/akismet/3.1.11/tl.zip";s:10:"autoupdate";b:1;}i:18;a:7:{s:4:"type";s:6:"plugin";s:4:"slug";s:7:"akismet";s:8:"language";s:5:"tr_TR";s:7:"version";s:3:"3.2";s:7:"updated";s:19:"2016-11-06 11:02:23";s:7:"package";s:72:"https://downloads.wordpress.org/translation/plugin/akismet/3.2/tr_TR.zip";s:10:"autoupdate";b:1;}i:19;a:7:{s:4:"type";s:6:"plugin";s:4:"slug";s:7:"akismet";s:8:"language";s:2:"uk";s:7:"version";s:3:"3.2";s:7:"updated";s:19:"2017-01-04 22:38:00";s:7:"package";s:69:"https://downloads.wordpress.org/translation/plugin/akismet/3.2/uk.zip";s:10:"autoupdate";b:1;}i:20;a:7:{s:4:"type";s:6:"plugin";s:4:"slug";s:7:"akismet";s:8:"language";s:5:"zh_CN";s:7:"version";s:6:"3.1.11";s:7:"updated";s:19:"2016-04-17 03:28:10";s:7:"package";s:75:"https://downloads.wordpress.org/translation/plugin/akismet/3.1.11/zh_CN.zip";s:10:"autoupdate";b:1;}i:21;a:7:{s:4:"type";s:6:"plugin";s:4:"slug";s:18:"birds-custom-login";s:8:"language";s:5:"fr_FR";s:7:"version";s:5:"1.0.8";s:7:"updated";s:19:"2016-09-02 13:05:58";s:7:"package";s:85:"https://downloads.wordpress.org/translation/plugin/birds-custom-login/1.0.8/fr_FR.zip";s:10:"autoupdate";b:1;}i:22;a:7:{s:4:"type";s:6:"plugin";s:4:"slug";s:18:"birds-custom-login";s:8:"language";s:5:"pt_PT";s:7:"version";s:5:"1.0.8";s:7:"updated";s:19:"2016-09-02 13:36:49";s:7:"package";s:85:"https://downloads.wordpress.org/translation/plugin/birds-custom-login/1.0.8/pt_PT.zip";s:10:"autoupdate";b:1;}i:23;a:7:{s:4:"type";s:6:"plugin";s:4:"slug";s:19:"custom-post-type-ui";s:8:"language";s:5:"de_DE";s:7:"version";s:5:"1.5.2";s:7:"updated";s:19:"2017-02-18 00:38:17";s:7:"package";s:86:"https://downloads.wordpress.org/translation/plugin/custom-post-type-ui/1.5.2/de_DE.zip";s:10:"autoupdate";b:1;}i:24;a:7:{s:4:"type";s:6:"plugin";s:4:"slug";s:19:"custom-post-type-ui";s:8:"language";s:5:"it_IT";s:7:"version";s:5:"1.5.2";s:7:"updated";s:19:"2017-02-02 11:39:37";s:7:"package";s:86:"https://downloads.wordpress.org/translation/plugin/custom-post-type-ui/1.5.2/it_IT.zip";s:10:"autoupdate";b:1;}i:25;a:7:{s:4:"type";s:6:"plugin";s:4:"slug";s:19:"custom-post-type-ui";s:8:"language";s:2:"ja";s:7:"version";s:5:"1.5.2";s:7:"updated";s:19:"2016-12-10 17:02:53";s:7:"package";s:83:"https://downloads.wordpress.org/translation/plugin/custom-post-type-ui/1.5.2/ja.zip";s:10:"autoupdate";b:1;}i:26;a:7:{s:4:"type";s:6:"plugin";s:4:"slug";s:19:"custom-post-type-ui";s:8:"language";s:5:"nl_NL";s:7:"version";s:5:"1.1.3";s:7:"updated";s:19:"2015-12-24 16:33:19";s:7:"package";s:86:"https://downloads.wordpress.org/translation/plugin/custom-post-type-ui/1.1.3/nl_NL.zip";s:10:"autoupdate";b:1;}i:27;a:7:{s:4:"type";s:6:"plugin";s:4:"slug";s:19:"custom-post-type-ui";s:8:"language";s:5:"pt_PT";s:7:"version";s:5:"1.5.2";s:7:"updated";s:19:"2017-03-10 14:59:42";s:7:"package";s:86:"https://downloads.wordpress.org/translation/plugin/custom-post-type-ui/1.5.2/pt_PT.zip";s:10:"autoupdate";b:1;}i:28;a:7:{s:4:"type";s:6:"plugin";s:4:"slug";s:14:"duplicate-post";s:8:"language";s:5:"da_DK";s:7:"version";s:5:"3.0.3";s:7:"updated";s:19:"2016-11-15 14:44:47";s:7:"package";s:81:"https://downloads.wordpress.org/translation/plugin/duplicate-post/3.0.3/da_DK.zip";s:10:"autoupdate";b:1;}i:29;a:7:{s:4:"type";s:6:"plugin";s:4:"slug";s:14:"duplicate-post";s:8:"language";s:5:"de_DE";s:7:"version";s:5:"3.1.2";s:7:"updated";s:19:"2017-02-07 09:11:21";s:7:"package";s:81:"https://downloads.wordpress.org/translation/plugin/duplicate-post/3.1.2/de_DE.zip";s:10:"autoupdate";b:1;}i:30;a:7:{s:4:"type";s:6:"plugin";s:4:"slug";s:14:"duplicate-post";s:8:"language";s:5:"es_ES";s:7:"version";s:5:"3.1.2";s:7:"updated";s:19:"2017-01-29 01:04:02";s:7:"package";s:81:"https://downloads.wordpress.org/translation/plugin/duplicate-post/3.1.2/es_ES.zip";s:10:"autoupdate";b:1;}i:31;a:7:{s:4:"type";s:6:"plugin";s:4:"slug";s:14:"duplicate-post";s:8:"language";s:5:"fr_FR";s:7:"version";s:5:"3.1.2";s:7:"updated";s:19:"2017-02-04 08:13:59";s:7:"package";s:81:"https://downloads.wordpress.org/translation/plugin/duplicate-post/3.1.2/fr_FR.zip";s:10:"autoupdate";b:1;}i:32;a:7:{s:4:"type";s:6:"plugin";s:4:"slug";s:14:"duplicate-post";s:8:"language";s:5:"it_IT";s:7:"version";s:5:"3.1.2";s:7:"updated";s:19:"2017-01-29 01:04:04";s:7:"package";s:81:"https://downloads.wordpress.org/translation/plugin/duplicate-post/3.1.2/it_IT.zip";s:10:"autoupdate";b:1;}i:33;a:7:{s:4:"type";s:6:"plugin";s:4:"slug";s:14:"duplicate-post";s:8:"language";s:2:"ja";s:7:"version";s:5:"3.1.2";s:7:"updated";s:19:"2016-12-26 09:14:20";s:7:"package";s:78:"https://downloads.wordpress.org/translation/plugin/duplicate-post/3.1.2/ja.zip";s:10:"autoupdate";b:1;}i:34;a:7:{s:4:"type";s:6:"plugin";s:4:"slug";s:14:"duplicate-post";s:8:"language";s:5:"nl_NL";s:7:"version";s:5:"3.0.3";s:7:"updated";s:19:"2016-10-12 11:57:21";s:7:"package";s:81:"https://downloads.wordpress.org/translation/plugin/duplicate-post/3.0.3/nl_NL.zip";s:10:"autoupdate";b:1;}i:35;a:7:{s:4:"type";s:6:"plugin";s:4:"slug";s:14:"duplicate-post";s:8:"language";s:5:"pt_BR";s:7:"version";s:5:"3.1.2";s:7:"updated";s:19:"2017-01-29 01:04:04";s:7:"package";s:81:"https://downloads.wordpress.org/translation/plugin/duplicate-post/3.1.2/pt_BR.zip";s:10:"autoupdate";b:1;}i:36;a:7:{s:4:"type";s:6:"plugin";s:4:"slug";s:14:"duplicate-post";s:8:"language";s:5:"pt_PT";s:7:"version";s:5:"3.1.2";s:7:"updated";s:19:"2016-12-13 18:25:27";s:7:"package";s:81:"https://downloads.wordpress.org/translation/plugin/duplicate-post/3.1.2/pt_PT.zip";s:10:"autoupdate";b:1;}i:37;a:7:{s:4:"type";s:6:"plugin";s:4:"slug";s:14:"duplicate-post";s:8:"language";s:5:"ru_RU";s:7:"version";s:5:"3.1.2";s:7:"updated";s:19:"2017-01-06 22:33:57";s:7:"package";s:81:"https://downloads.wordpress.org/translation/plugin/duplicate-post/3.1.2/ru_RU.zip";s:10:"autoupdate";b:1;}i:38;a:7:{s:4:"type";s:6:"plugin";s:4:"slug";s:14:"duplicate-post";s:8:"language";s:5:"sv_SE";s:7:"version";s:5:"3.1.2";s:7:"updated";s:19:"2017-01-29 01:04:05";s:7:"package";s:81:"https://downloads.wordpress.org/translation/plugin/duplicate-post/3.1.2/sv_SE.zip";s:10:"autoupdate";b:1;}i:39;a:7:{s:4:"type";s:6:"plugin";s:4:"slug";s:14:"duplicate-post";s:8:"language";s:5:"tr_TR";s:7:"version";s:5:"3.0.3";s:7:"updated";s:19:"2016-10-09 19:24:31";s:7:"package";s:81:"https://downloads.wordpress.org/translation/plugin/duplicate-post/3.0.3/tr_TR.zip";s:10:"autoupdate";b:1;}i:40;a:7:{s:4:"type";s:6:"plugin";s:4:"slug";s:18:"password-protected";s:8:"language";s:5:"de_DE";s:7:"version";s:5:"2.0.3";s:7:"updated";s:19:"2016-10-07 21:47:34";s:7:"package";s:85:"https://downloads.wordpress.org/translation/plugin/password-protected/2.0.3/de_DE.zip";s:10:"autoupdate";b:1;}i:41;a:7:{s:4:"type";s:6:"plugin";s:4:"slug";s:18:"password-protected";s:8:"language";s:5:"es_ES";s:7:"version";s:5:"2.0.3";s:7:"updated";s:19:"2016-04-17 19:24:12";s:7:"package";s:85:"https://downloads.wordpress.org/translation/plugin/password-protected/2.0.3/es_ES.zip";s:10:"autoupdate";b:1;}i:42;a:7:{s:4:"type";s:6:"plugin";s:4:"slug";s:18:"password-protected";s:8:"language";s:5:"fr_FR";s:7:"version";s:5:"2.0.3";s:7:"updated";s:19:"2016-06-10 10:29:07";s:7:"package";s:85:"https://downloads.wordpress.org/translation/plugin/password-protected/2.0.3/fr_FR.zip";s:10:"autoupdate";b:1;}i:43;a:7:{s:4:"type";s:6:"plugin";s:4:"slug";s:18:"password-protected";s:8:"language";s:2:"ja";s:7:"version";s:5:"2.0.3";s:7:"updated";s:19:"2016-12-14 08:53:50";s:7:"package";s:82:"https://downloads.wordpress.org/translation/plugin/password-protected/2.0.3/ja.zip";s:10:"autoupdate";b:1;}i:44;a:7:{s:4:"type";s:6:"plugin";s:4:"slug";s:18:"password-protected";s:8:"language";s:5:"nl_NL";s:7:"version";s:5:"2.0.3";s:7:"updated";s:19:"2016-11-27 17:57:25";s:7:"package";s:85:"https://downloads.wordpress.org/translation/plugin/password-protected/2.0.3/nl_NL.zip";s:10:"autoupdate";b:1;}i:45;a:7:{s:4:"type";s:6:"plugin";s:4:"slug";s:18:"password-protected";s:8:"language";s:5:"pt_PT";s:7:"version";s:5:"2.0.3";s:7:"updated";s:19:"2017-01-06 13:13:01";s:7:"package";s:85:"https://downloads.wordpress.org/translation/plugin/password-protected/2.0.3/pt_PT.zip";s:10:"autoupdate";b:1;}i:46;a:7:{s:4:"type";s:6:"plugin";s:4:"slug";s:21:"regenerate-thumbnails";s:8:"language";s:5:"de_DE";s:7:"version";s:5:"2.2.6";s:7:"updated";s:19:"2016-05-31 07:37:54";s:7:"package";s:88:"https://downloads.wordpress.org/translation/plugin/regenerate-thumbnails/2.2.6/de_DE.zip";s:10:"autoupdate";b:1;}i:47;a:7:{s:4:"type";s:6:"plugin";s:4:"slug";s:21:"regenerate-thumbnails";s:8:"language";s:2:"el";s:7:"version";s:5:"2.2.6";s:7:"updated";s:19:"2016-02-02 22:28:28";s:7:"package";s:85:"https://downloads.wordpress.org/translation/plugin/regenerate-thumbnails/2.2.6/el.zip";s:10:"autoupdate";b:1;}i:48;a:7:{s:4:"type";s:6:"plugin";s:4:"slug";s:21:"regenerate-thumbnails";s:8:"language";s:5:"es_ES";s:7:"version";s:5:"2.2.6";s:7:"updated";s:19:"2016-12-14 13:10:34";s:7:"package";s:88:"https://downloads.wordpress.org/translation/plugin/regenerate-thumbnails/2.2.6/es_ES.zip";s:10:"autoupdate";b:1;}i:49;a:7:{s:4:"type";s:6:"plugin";s:4:"slug";s:21:"regenerate-thumbnails";s:8:"language";s:2:"fi";s:7:"version";s:5:"2.2.6";s:7:"updated";s:19:"2016-03-21 14:07:53";s:7:"package";s:85:"https://downloads.wordpress.org/translation/plugin/regenerate-thumbnails/2.2.6/fi.zip";s:10:"autoupdate";b:1;}i:50;a:7:{s:4:"type";s:6:"plugin";s:4:"slug";s:21:"regenerate-thumbnails";s:8:"language";s:5:"fr_FR";s:7:"version";s:5:"2.2.6";s:7:"updated";s:19:"2017-03-03 10:55:17";s:7:"package";s:88:"https://downloads.wordpress.org/translation/plugin/regenerate-thumbnails/2.2.6/fr_FR.zip";s:10:"autoupdate";b:1;}i:51;a:7:{s:4:"type";s:6:"plugin";s:4:"slug";s:21:"regenerate-thumbnails";s:8:"language";s:5:"it_IT";s:7:"version";s:5:"2.2.6";s:7:"updated";s:19:"2016-12-20 05:16:53";s:7:"package";s:88:"https://downloads.wordpress.org/translation/plugin/regenerate-thumbnails/2.2.6/it_IT.zip";s:10:"autoupdate";b:1;}i:52;a:7:{s:4:"type";s:6:"plugin";s:4:"slug";s:21:"regenerate-thumbnails";s:8:"language";s:2:"ja";s:7:"version";s:5:"2.2.6";s:7:"updated";s:19:"2015-12-13 13:21:47";s:7:"package";s:85:"https://downloads.wordpress.org/translation/plugin/regenerate-thumbnails/2.2.6/ja.zip";s:10:"autoupdate";b:1;}i:53;a:7:{s:4:"type";s:6:"plugin";s:4:"slug";s:21:"regenerate-thumbnails";s:8:"language";s:5:"nl_NL";s:7:"version";s:5:"2.2.6";s:7:"updated";s:19:"2016-11-03 18:06:11";s:7:"package";s:88:"https://downloads.wordpress.org/translation/plugin/regenerate-thumbnails/2.2.6/nl_NL.zip";s:10:"autoupdate";b:1;}i:54;a:7:{s:4:"type";s:6:"plugin";s:4:"slug";s:21:"regenerate-thumbnails";s:8:"language";s:5:"pl_PL";s:7:"version";s:5:"2.2.6";s:7:"updated";s:19:"2015-10-20 20:07:16";s:7:"package";s:88:"https://downloads.wordpress.org/translation/plugin/regenerate-thumbnails/2.2.6/pl_PL.zip";s:10:"autoupdate";b:1;}i:55;a:7:{s:4:"type";s:6:"plugin";s:4:"slug";s:21:"regenerate-thumbnails";s:8:"language";s:5:"pt_BR";s:7:"version";s:5:"2.2.6";s:7:"updated";s:19:"2016-06-11 04:43:44";s:7:"package";s:88:"https://downloads.wordpress.org/translation/plugin/regenerate-thumbnails/2.2.6/pt_BR.zip";s:10:"autoupdate";b:1;}i:56;a:7:{s:4:"type";s:6:"plugin";s:4:"slug";s:21:"regenerate-thumbnails";s:8:"language";s:5:"pt_PT";s:7:"version";s:5:"2.2.6";s:7:"updated";s:19:"2017-01-20 08:36:15";s:7:"package";s:88:"https://downloads.wordpress.org/translation/plugin/regenerate-thumbnails/2.2.6/pt_PT.zip";s:10:"autoupdate";b:1;}i:57;a:7:{s:4:"type";s:6:"plugin";s:4:"slug";s:21:"regenerate-thumbnails";s:8:"language";s:5:"ru_RU";s:7:"version";s:5:"2.2.6";s:7:"updated";s:19:"2016-11-01 14:37:55";s:7:"package";s:88:"https://downloads.wordpress.org/translation/plugin/regenerate-thumbnails/2.2.6/ru_RU.zip";s:10:"autoupdate";b:1;}i:58;a:7:{s:4:"type";s:6:"plugin";s:4:"slug";s:21:"regenerate-thumbnails";s:8:"language";s:5:"sv_SE";s:7:"version";s:5:"2.2.6";s:7:"updated";s:19:"2016-03-11 13:23:58";s:7:"package";s:88:"https://downloads.wordpress.org/translation/plugin/regenerate-thumbnails/2.2.6/sv_SE.zip";s:10:"autoupdate";b:1;}i:59;a:7:{s:4:"type";s:6:"plugin";s:4:"slug";s:14:"social-warfare";s:8:"language";s:5:"fr_FR";s:7:"version";s:5:"2.2.3";s:7:"updated";s:19:"2016-11-29 19:54:52";s:7:"package";s:81:"https://downloads.wordpress.org/translation/plugin/social-warfare/2.2.3/fr_FR.zip";s:10:"autoupdate";b:1;}i:60;a:7:{s:4:"type";s:6:"plugin";s:4:"slug";s:14:"social-warfare";s:8:"language";s:5:"pl_PL";s:7:"version";s:5:"2.2.3";s:7:"updated";s:19:"2017-01-04 12:52:45";s:7:"package";s:81:"https://downloads.wordpress.org/translation/plugin/social-warfare/2.2.3/pl_PL.zip";s:10:"autoupdate";b:1;}}s:9:"no_update";a:9:{s:19:"akismet/akismet.php";O:8:"stdClass":6:{s:2:"id";s:2:"15";s:4:"slug";s:7:"akismet";s:6:"plugin";s:19:"akismet/akismet.php";s:11:"new_version";s:3:"3.3";s:3:"url";s:38:"https://wordpress.org/plugins/akismet/";s:7:"package";s:54:"https://downloads.wordpress.org/plugin/akismet.3.3.zip";}s:41:"birds-custom-login/birds-custom-login.php";O:8:"stdClass":6:{s:2:"id";s:5:"57532";s:4:"slug";s:18:"birds-custom-login";s:6:"plugin";s:41:"birds-custom-login/birds-custom-login.php";s:11:"new_version";s:5:"1.0.8";s:3:"url";s:49:"https://wordpress.org/plugins/birds-custom-login/";s:7:"package";s:67:"https://downloads.wordpress.org/plugin/birds-custom-login.1.0.8.zip";}s:43:"custom-post-type-ui/custom-post-type-ui.php";O:8:"stdClass":6:{s:2:"id";s:5:"13183";s:4:"slug";s:19:"custom-post-type-ui";s:6:"plugin";s:43:"custom-post-type-ui/custom-post-type-ui.php";s:11:"new_version";s:5:"1.5.2";s:3:"url";s:50:"https://wordpress.org/plugins/custom-post-type-ui/";s:7:"package";s:68:"https://downloads.wordpress.org/plugin/custom-post-type-ui.1.5.2.zip";}s:33:"duplicate-post/duplicate-post.php";O:8:"stdClass":7:{s:2:"id";s:4:"1295";s:4:"slug";s:14:"duplicate-post";s:6:"plugin";s:33:"duplicate-post/duplicate-post.php";s:11:"new_version";s:5:"3.1.2";s:3:"url";s:45:"https://wordpress.org/plugins/duplicate-post/";s:7:"package";s:63:"https://downloads.wordpress.org/plugin/duplicate-post.3.1.2.zip";s:14:"upgrade_notice";s:36:"Fixes the problem with custom fields";}s:31:"featured-post/featured-post.php";O:8:"stdClass":6:{s:2:"id";s:5:"15626";s:4:"slug";s:13:"featured-post";s:6:"plugin";s:31:"featured-post/featured-post.php";s:11:"new_version";s:5:"3.2.1";s:3:"url";s:44:"https://wordpress.org/plugins/featured-post/";s:7:"package";s:62:"https://downloads.wordpress.org/plugin/featured-post.3.2.1.zip";}s:9:"hello.php";O:8:"stdClass":6:{s:2:"id";s:4:"3564";s:4:"slug";s:11:"hello-dolly";s:6:"plugin";s:9:"hello.php";s:11:"new_version";s:3:"1.6";s:3:"url";s:42:"https://wordpress.org/plugins/hello-dolly/";s:7:"package";s:58:"https://downloads.wordpress.org/plugin/hello-dolly.1.6.zip";}s:41:"password-protected/password-protected.php";O:8:"stdClass":7:{s:2:"id";s:5:"28951";s:4:"slug";s:18:"password-protected";s:6:"plugin";s:41:"password-protected/password-protected.php";s:11:"new_version";s:5:"2.0.3";s:3:"url";s:49:"https://wordpress.org/plugins/password-protected/";s:7:"package";s:67:"https://downloads.wordpress.org/plugin/password-protected.2.0.3.zip";s:14:"upgrade_notice";s:144:"Show user&#039;s IP address beside &quot;Allow IP Addresses&quot; admin setting. Declare methods as public or private and use PHP5 constructors.";}s:47:"regenerate-thumbnails/regenerate-thumbnails.php";O:8:"stdClass":6:{s:2:"id";s:4:"4437";s:4:"slug";s:21:"regenerate-thumbnails";s:6:"plugin";s:47:"regenerate-thumbnails/regenerate-thumbnails.php";s:11:"new_version";s:5:"2.2.6";s:3:"url";s:52:"https://wordpress.org/plugins/regenerate-thumbnails/";s:7:"package";s:64:"https://downloads.wordpress.org/plugin/regenerate-thumbnails.zip";}s:33:"social-warfare/social-warfare.php";O:8:"stdClass":6:{s:2:"id";s:5:"73715";s:4:"slug";s:14:"social-warfare";s:6:"plugin";s:33:"social-warfare/social-warfare.php";s:11:"new_version";s:5:"2.2.3";s:3:"url";s:45:"https://wordpress.org/plugins/social-warfare/";s:7:"package";s:57:"https://downloads.wordpress.org/plugin/social-warfare.zip";}}}', 'no'),
(456, '_site_transient_timeout_theme_roots', '1490300705', 'no'),
(457, '_site_transient_theme_roots', 'a:1:{s:12:"ferrycorsten";s:7:"/themes";}', 'no'),
(458, '_site_transient_update_themes', 'O:8:"stdClass":4:{s:12:"last_checked";i:1490298905;s:7:"checked";a:1:{s:12:"ferrycorsten";s:3:"1.0";}s:8:"response";a:0:{}s:12:"translations";a:0:{}}', 'no'),
(459, 'can_compress_scripts', '1', 'no'),
(460, '_transient_timeout_plugin_slugs', '1490385809', 'no'),
(461, '_transient_plugin_slugs', 'a:12:{i:0;s:34:"advanced-custom-fields-pro/acf.php";i:1;s:19:"akismet/akismet.php";i:2;s:41:"birds-custom-login/birds-custom-login.php";i:3;s:43:"custom-post-type-ui/custom-post-type-ui.php";i:4;s:33:"duplicate-post/duplicate-post.php";i:5;s:31:"featured-post/featured-post.php";i:6;s:29:"gravityforms/gravityforms.php";i:7;s:9:"hello.php";i:8;s:41:"password-protected/password-protected.php";i:9;s:47:"regenerate-thumbnails/regenerate-thumbnails.php";i:10;s:33:"social-warfare/social-warfare.php";i:11;s:39:"wp-migrate-db-pro/wp-migrate-db-pro.php";}', 'no'),
(462, '_site_transient_timeout_wpmdb_licence_response', '1490342609', 'no') ;
INSERT INTO `blingbling_options` ( `option_id`, `option_name`, `option_value`, `autoload`) VALUES
(463, '_site_transient_wpmdb_licence_response', '{"message":"<style type=\\"text\\/css\\" media=\\"screen\\">body .support .support-content{overflow:hidden;width:727px}body .support .support-content .intro{margin-bottom:20px}body .support .support-content .submission-success p,body .support .support-content .submission-error p{padding:2px;margin:0.5em 0;font-size:13px;line-height:1.5}body .support .support-content .dbrains-support-form{width:475px;float:left}body .support .support-content .dbrains-support-form p{width:auto}body .support .support-content .dbrains-support-form .field{margin-bottom:5px}body .support .support-content .dbrains-support-form input[type=text],body .support .support-content .dbrains-support-form textarea{width:100%}body .support .support-content .dbrains-support-form .field.from label{float:left;line-height:28px;display:block;font-weight:bold}body .support .support-content .dbrains-support-form .field.from select{float:right;width:400px}body .support .support-content .dbrains-support-form .field.from .note{clear:both;padding-top:5px}body .support .support-content .dbrains-support-form .field.email-message textarea{height:170px}body .support .support-content .dbrains-support-form .field.remote-diagnostic-content{padding-left:20px}body .support .support-content .dbrains-support-form .field.remote-diagnostic-content ol{margin:0 0 5px 20px}body .support .support-content .dbrains-support-form .field.remote-diagnostic-content li{font-size:12px;color:#666;margin-bottom:0;line-height:1.4em}body .support .support-content .dbrains-support-form .field.remote-diagnostic-content textarea{height:80px}body .support .support-content .dbrains-support-form .note{font-size:12px;color:#666}body .support .support-content .dbrains-support-form .submit-form{overflow:hidden;padding:10px 0}body .support .support-content .dbrains-support-form .button{float:left}body .support .support-content .dbrains-support-form .button:active,body .support .support-content .dbrains-support-form .button:focus{outline:none}body .support .support-content .dbrains-support-form .ajax-spinner{float:left;margin-left:5px;margin-top:3px}body .support .support-content .additional-help{float:right;width:220px}body .support .support-content .additional-help a{text-decoration:none}body .support .support-content .additional-help h1{margin:0 0 12px 0;padding:0;font-size:18px;font-weight:normal;line-height:1}body .support .support-content .additional-help h1 a{color:#333}body .support .support-content .additional-help .docs{background-color:#e6e6e6;padding:15px 15px 10px 15px}body .support .support-content .additional-help .docs ul{margin:0}body .support .support-content .additional-help .docs li{font-size:14px}\\n<\\/style><section class=\\"dbrains-support-form\\">\\n\\n<p class=\\"intro\\">\\n\\tYou have an active <strong>Developer<\\/strong> license. You will get front-of-the-line email support service when submitting the form below.<\\/p>\\n\\n<div class=\\"updated submission-success\\" style=\\"display: none;\\">\\n\\t<p><strong>Success!<\\/strong> &mdash; Thanks for submitting your support request. We\'ll be in touch soon.<\\/p>\\n<\\/div>\\n\\n<div class=\\"error submission-error api-error\\" style=\\"display: none;\\">\\n\\t<p><strong>Error!<\\/strong> &mdash; <\\/p>\\n<\\/div>\\n\\n<div class=\\"error submission-error xhr-error\\" style=\\"display: none;\\">\\n\\t<p><strong>Error!<\\/strong> &mdash; There was a problem submitting your request:<\\/p>\\n<\\/div>\\n\\n<div class=\\"error submission-error email-error\\" style=\\"display: none;\\">\\n\\t<p><strong>Error!<\\/strong> &mdash; Please select your email address.<\\/p>\\n<\\/div>\\n\\n<div class=\\"error submission-error subject-error\\" style=\\"display: none;\\">\\n\\t<p><strong>Error!<\\/strong> &mdash; Please enter a subject.<\\/p>\\n<\\/div>\\n\\n<div class=\\"error submission-error message-error\\" style=\\"display: none;\\">\\n\\t<p><strong>Error!<\\/strong> &mdash; Please enter a message.<\\/p>\\n<\\/div>\\n\\n<div class=\\"error submission-error remote-diagnostic-content-error\\" style=\\"display: none;\\">\\n\\t<p><strong>Error!<\\/strong> &mdash; Please paste in the Diagnostic Info &amp; Error Log from your <strong>remote site<\\/strong>.<\\/p>\\n<\\/div>\\n\\n<div class=\\"error submission-error both-diagnostic-same-error\\" style=\\"display: none;\\">\\n\\t<p><strong>Error!<\\/strong> &mdash; Looks like you pasted the local Diagnostic Info &amp; Error Log into the textbox for the remote info. Please get the info for your <strong>remote site<\\/strong> and paste it in, or just uncheck the second checkbox if you&#8217;d rather not include your remote site info.<\\/p>\\n<\\/div>\\n\\n<form target=\\"_blank\\" method=\\"post\\" action=\\"https:\\/\\/api.deliciousbrains.com\\/?wc-api=delicious-brains&request=submit_support_request&licence_key=954a659e-2634-494f-947a-893d17f63321&product=wp-migrate-db-pro\\">\\n\\n\\t<div class=\\"field from\\">\\n\\t\\t<label>From:<\\/label>\\n\\t\\t<select name=\\"email\\">\\n\\t\\t<option value=\\"\\">&mdash; Select your email address &mdash;<\\/option>\\n\\t\\t<option value=\\"nicholas@manufactur.co\\">nicholas@manufactur.co<\\/option>\\t\\t<\\/select>\\n\\n\\t\\t<p class=\\"note\\">\\n\\t\\t\\tReplies will be sent to this email address. Update your name &amp; email in <a href=\\"https:\\/\\/deliciousbrains.com\\/my-account\\/\\">My Account<\\/a>.\\t\\t<\\/p>\\n\\t<\\/div>\\n\\n\\t<div class=\\"field subject\\">\\n\\t\\t<input type=\\"text\\" name=\\"subject\\" placeholder=\\"Subject\\">\\n\\t<\\/div>\\n\\n\\t<div class=\\"field email-message\\">\\n\\t\\t<textarea name=\\"message\\" placeholder=\\"Message\\"><\\/textarea>\\n\\t<\\/div>\\n\\n\\t<div class=\\"field checkbox local-diagnostic\\">\\n\\t\\t<label>\\n\\t\\t\\t<input type=\\"checkbox\\" name=\\"local-diagnostic\\" value=\\"1\\" checked>\\n\\t\\t\\tAttach <strong>this site&#8217;s<\\/strong> Diagnostic Info &amp; Error Log (below)\\t\\t<\\/label>\\n\\t<\\/div>\\n\\t\\t<div class=\\"field checkbox remote-diagnostic\\">\\n\\t\\t<label>\\n\\t\\t\\t<input type=\\"checkbox\\" name=\\"remote-diagnostic\\" value=\\"1\\" checked>\\n\\t\\t\\tAttach the <strong>remote site&#8217;s<\\/strong> Diagnostic Info &amp; Error Log\\t\\t<\\/label>\\n\\t<\\/div>\\n\\n\\t<div class=\\"field remote-diagnostic-content\\">\\n\\t\\t<ol>\\n\\t\\t\\t<li>Go to the Help tab of the remote site<\\/li>\\n\\t\\t\\t<li>Copy the Diagnostic Info &amp; Error Log<\\/li>\\n\\t\\t\\t<li>Paste it below<\\/li>\\n\\t\\t<\\/ol>\\n\\t\\t<textarea name=\\"remote-diagnostic-content\\" placeholder=\\"Remote site&#8217;s Diagnostic Info &amp; Error Log\\"><\\/textarea>\\n\\t<\\/div>\\n\\t\\t<div class=\\"submit-form\\">\\n\\t\\t<button type=\\"submit\\" class=\\"button\\">Send Email<\\/button>\\n\\t<\\/div>\\n\\n\\t<p class=\\"note trouble\\">\\n\\t\\tHaving trouble submitting the form? Email your support request to <a href=\\"mailto:priority-wpmdb@deliciousbrains.com\\">priority-wpmdb@deliciousbrains.com<\\/a> instead.\\t<\\/p>\\n\\n<\\/form>\\n\\n<\\/section>\\n\\n\\t<aside class=\\"additional-help\\">\\n\\t\\t<section class=\\"docs\\">\\n\\t\\t\\t<h1><a href=\\"https:\\/\\/deliciousbrains.com\\/wp-migrate-db-pro\\/docs\\/\\">Documentation<\\/a><\\/h1>\\n\\t\\t\\t<ul class=\\"categories\\">\\n\\t\\t\\t\\t<li><a href=\\"https:\\/\\/deliciousbrains.com\\/wp-migrate-db-pro\\/docs\\/getting-started\\/\\">Getting Started<\\/a><\\/li><li><a href=\\"https:\\/\\/deliciousbrains.com\\/wp-migrate-db-pro\\/docs\\/debugging\\/\\">Debugging<\\/a><\\/li><li><a href=\\"https:\\/\\/deliciousbrains.com\\/wp-migrate-db-pro\\/docs\\/cli\\/\\">CLI<\\/a><\\/li><li><a href=\\"https:\\/\\/deliciousbrains.com\\/wp-migrate-db-pro\\/docs\\/common-errors\\/\\">Common Errors<\\/a><\\/li><li><a href=\\"https:\\/\\/deliciousbrains.com\\/wp-migrate-db-pro\\/docs\\/howto\\/\\">How To<\\/a><\\/li><li><a href=\\"https:\\/\\/deliciousbrains.com\\/wp-migrate-db-pro\\/docs\\/addons\\/\\">Addons<\\/a><\\/li><li><a href=\\"https:\\/\\/deliciousbrains.com\\/wp-migrate-db-pro\\/docs\\/multisite\\/\\">Multisite<\\/a><\\/li><li><a href=\\"https:\\/\\/deliciousbrains.com\\/wp-migrate-db-pro\\/docs\\/changelogs\\/\\">Changelogs<\\/a><\\/li>\\t\\t\\t<\\/ul>\\n\\t\\t<\\/section>\\n\\t<\\/aside>\\n<script>!function(a){var b=a(\\".dbrains-support-form form\\"),c=a(\\".submit-form\\",b);is_submitting=!1;var d=a(\\".remote-diagnostic input\\",b),e=a(\\".remote-diagnostic-content\\",b);d.on(\\"click\\",function(){d.prop(\\"checked\\")?e.show():e.hide()});var f=ajaxurl.replace(\\"\\/admin-ajax.php\\",\\"\\/images\\/wpspin_light\\");window.devicePixelRatio>=2&&(f+=\\"-2x\\"),f+=\\".gif\\",b.submit(function(d){if(d.preventDefault(),!is_submitting){is_submitting=!0,a(\\".button\\",b).blur();var e=a(\\".ajax-spinner\\",c);e[0]?e.show():(e=a(\'<img src=\\"\'+f+\'\\" alt=\\"\\" class=\\"ajax-spinner general-spinner\\" \\/>\'),c.append(e)),a(\\".submission-error\\").hide();var g=[\\"email\\",\\"subject\\",\\"message\\"],h={},i=!1;a.each(b.serializeArray(),function(b,c){h[c.name]=c.value,a.inArray(c.name,g)>-1&&\\"\\"===c.value&&(a(\\".\\"+c.name+\\"-error\\").fadeIn(),i=!0)});var j=a(\\"input[name=remote-diagnostic]\\",b).is(\\":checked\\");if(j)if(\\"\\"===h[\\"remote-diagnostic-content\\"])a(\\".remote-diagnostic-content-error\\").fadeIn(),i=!0;else{var k=h[\\"remote-diagnostic-content\\"].substr(0,h[\\"remote-diagnostic-content\\"].indexOf(\\"\\\\n\\")),l=a(\\".debug-log-textarea\\")[0],m=l.value.substr(0,l.value.indexOf(\\"\\\\n\\"));console.log(\'\\"\'+k+\'\\"\'),console.log(\'\\"\'+m+\'\\"\'),k.trim()==m.trim()&&(a(\\".both-diagnostic-same-error\\").fadeIn(),i=!0)}if(i)return e.hide(),void(is_submitting=!1);j||(h[\\"remote-diagnostic-content\\"]=\\"\\"),a(\\"input[name=local-diagnostic]\\",b).is(\\":checked\\")&&(h[\\"local-diagnostic-content\\"]=a(\\".debug-log-textarea\\").val()),a.ajax({url:b.prop(\\"action\\"),type:\\"POST\\",dataType:\\"JSON\\",cache:!1,data:h,error:function(b,c,d){var f=a(\\".xhr-error\\");a(\\"p\\",f).append(\\" \\"+d+\\" (\\"+c+\\")\\"),f.show(),e.hide(),is_submitting=!1},success:function(c){if(\\"undefined\\"!=typeof c.errors){var d=a(\\".api-error\\");return a.each(c.errors,function(b,c){return a(\\"p\\",d).append(c),!1}),d.show(),e.hide(),void(is_submitting=!1)}a(\\".submission-success\\").show(),b.hide(),e.hide(),is_submitting=!1}})}})}(jQuery);<\\/script>","addons_available":"1","addons_available_list":{"wp-migrate-db-pro-media-files":2351,"wp-migrate-db-pro-cli":3948,"wp-migrate-db-pro-multisite-tools":7999},"addon_list":{"wp-migrate-db-pro-media-files":{"name":"Media Files","desc":"Allows you to push and pull your files in the Media Library between two WordPress installs. It can compare both libraries and only migrate those missing or updated, or it can do a complete copy of one site\\u2019s library to another. <a style=\\"color:#fff;text-decoration:underline;\\" href=\\"https:\\/\\/deliciousbrains.com\\/wp-migrate-db-pro\\/doc\\/media-files-addon\\/\\">More Details &rarr;<\\/a>","version":"1.4.7","beta_version":"","tested":"4.7.3"},"wp-migrate-db-pro-cli":{"name":"CLI","desc":"Integrates WP Migrate DB Pro with WP-CLI allowing you to run migrations from the command line: <code>wp migratedb &lt;push|pull&gt; &lt;url&gt; &lt;secret-key&gt;<\\/code> <code>[--find=&lt;strings&gt;] [--replace=&lt;strings&gt;] ...<\\/code> <a href=\\"https:\\/\\/deliciousbrains.com\\/wp-migrate-db-pro\\/doc\\/cli-addon\\/\\">More Details &rarr;<\\/a>","version":"1.3","beta_version":"","required":"1.4b1","tested":"4.7.3"},"wp-migrate-db-pro-multisite-tools":{"name":"Multisite Tools","desc":"Export a subsite as an SQL file that can then be imported as a single site install. <a href=\\"https:\\/\\/deliciousbrains.com\\/wp-migrate-db-pro\\/doc\\/multisite-tools-addon\\/\\">More Details &rarr;<\\/a>","version":"1.1.5","beta_version":"","required":"1.5-dev","tested":"4.7.3"}}}', 'no'),
(465, 'wpmdb_state_timeout_58d441c07ee68', '1490391873', 'no'),
(466, 'wpmdb_state_58d441c07ee68', 'a:25:{s:6:"action";s:19:"wpmdb_migrate_table";s:6:"intent";s:4:"pull";s:3:"url";s:34:"https://ferrycorsten.manufactur.co";s:3:"key";s:40:"QJFqegtXBsJhhGdoFd67Jx4M0MfACVNSPj81QbOH";s:9:"form_data";s:762:"save_computer=1&gzip_file=1&action=pull&connection_info=https%3A%2F%2Fferrycorsten.manufactur.co%0D%0AQJFqegtXBsJhhGdoFd67Jx4M0MfACVNSPj81QbOH&replace_old%5B%5D=&replace_new%5B%5D=&replace_old%5B%5D=%2F%2Fferrycorsten.manufactur.co&replace_new%5B%5D=%2F%2Flocalhost%3A8888&replace_old%5B%5D=%2Fvar%2Fwww%2Fvhosts%2Ffggs-mvlh.accessdomain.com%2Fferrycorsten.manufactur.co&replace_new%5B%5D=%2FUsers%2Fchameleon_xing%2FWork%2FManufactur%2FDEV+Root%2Fferrycorsten.manufactur.co&table_migrate_option=migrate_only_with_prefix&replace_guids=1&exclude_transients=1&compatibility_older_mysql=1&create_backup=1&backup_option=backup_only_with_prefix&save_migration_profile=1&save_migration_profile_option=new&create_new_profile=ferrycorsten.manufactur.co&remote_json_data=";s:5:"stage";s:6:"backup";s:5:"nonce";s:10:"2c93efd3ca";s:12:"site_details";a:2:{s:5:"local";a:9:{s:12:"is_multisite";s:5:"false";s:8:"site_url";s:21:"http://localhost:8888";s:8:"home_url";s:21:"http://localhost:8888";s:6:"prefix";s:11:"blingbling_";s:15:"uploads_baseurl";s:41:"http://localhost:8888/wp-content/uploads/";s:7:"uploads";a:6:{s:4:"path";s:100:"/Users/chameleon_xing/Work/Manufactur/DEV Root/ferrycorsten.manufactur.co/wp-content/uploads/2017/03";s:3:"url";s:48:"http://localhost:8888/wp-content/uploads/2017/03";s:6:"subdir";s:8:"/2017/03";s:7:"basedir";s:92:"/Users/chameleon_xing/Work/Manufactur/DEV Root/ferrycorsten.manufactur.co/wp-content/uploads";s:7:"baseurl";s:40:"http://localhost:8888/wp-content/uploads";s:5:"error";b:0;}s:11:"uploads_dir";s:33:"wp-content/uploads/wp-migrate-db/";s:8:"subsites";a:0:{}s:13:"subsites_info";a:0:{}}s:6:"remote";a:9:{s:12:"is_multisite";s:5:"false";s:8:"site_url";s:34:"https://ferrycorsten.manufactur.co";s:8:"home_url";s:33:"http://ferrycorsten.manufactur.co";s:6:"prefix";s:11:"blingbling_";s:15:"uploads_baseurl";s:53:"http://ferrycorsten.manufactur.co/wp-content/uploads/";s:7:"uploads";a:6:{s:4:"path";s:96:"/var/www/vhosts/fggs-mvlh.accessdomain.com/ferrycorsten.manufactur.co/wp-content/uploads/2017/03";s:3:"url";s:60:"http://ferrycorsten.manufactur.co/wp-content/uploads/2017/03";s:6:"subdir";s:8:"/2017/03";s:7:"basedir";s:88:"/var/www/vhosts/fggs-mvlh.accessdomain.com/ferrycorsten.manufactur.co/wp-content/uploads";s:7:"baseurl";s:52:"http://ferrycorsten.manufactur.co/wp-content/uploads";s:5:"error";b:0;}s:11:"uploads_dir";s:33:"wp-content/uploads/wp-migrate-db/";s:8:"subsites";a:0:{}s:13:"subsites_info";a:0:{}}}s:11:"temp_prefix";s:5:"_mig_";s:5:"error";i:0;s:15:"remote_state_id";s:13:"58d441c06e307";s:13:"dump_filename";s:40:"wordpress_f8-backup-20170323214432-pstxz";s:8:"dump_url";s:99:"http://localhost:8888/wp-content/uploads/wp-migrate-db/wordpress_f8-backup-20170323214432-pstxz.sql";s:10:"db_version";s:6:"5.6.28";s:8:"site_url";s:21:"http://localhost:8888";s:18:"find_replace_pairs";a:2:{s:11:"replace_old";a:2:{i:1;s:28:"//ferrycorsten.manufactur.co";i:2;s:69:"/var/www/vhosts/fggs-mvlh.accessdomain.com/ferrycorsten.manufactur.co";}s:11:"replace_new";a:2:{i:1;s:16:"//localhost:8888";i:2;s:73:"/Users/chameleon_xing/Work/Manufactur/DEV Root/ferrycorsten.manufactur.co";}}s:18:"migration_state_id";s:13:"58d441c07ee68";s:5:"table";s:18:"blingbling_options";s:11:"current_row";s:0:"";s:10:"last_table";s:1:"0";s:12:"primary_keys";s:0:"";s:4:"gzip";i:1;s:10:"bottleneck";d:1048576;s:6:"prefix";s:11:"blingbling_";s:16:"dumpfile_created";b:1;}', 'no') ;

#
# End of data contents of table `blingbling_options`
# --------------------------------------------------------



#
# Delete any existing table `blingbling_postmeta`
#

DROP TABLE IF EXISTS `blingbling_postmeta`;


#
# Table structure of table `blingbling_postmeta`
#

CREATE TABLE `blingbling_postmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `post_id` (`post_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB AUTO_INCREMENT=391 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


#
# Data contents of table `blingbling_postmeta`
#
INSERT INTO `blingbling_postmeta` ( `meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1, 2, '_wp_page_template', 'default'),
(2, 2, '_wp_trash_meta_status', 'publish'),
(3, 2, '_wp_trash_meta_time', '1489419617'),
(4, 2, '_wp_desired_post_slug', 'sample-page'),
(5, 5, '_edit_last', '1'),
(6, 5, '_edit_lock', '1490275424:1'),
(7, 5, '_wp_page_template', 'page-home.php'),
(8, 7, '_edit_last', '1'),
(9, 7, '_edit_lock', '1490277966:1'),
(10, 1, '_wp_trash_meta_status', 'publish'),
(11, 1, '_wp_trash_meta_time', '1490016668'),
(12, 1, '_wp_desired_post_slug', 'hello-world'),
(13, 1, '_wp_trash_meta_comments_status', 'a:1:{i:1;s:1:"1";}'),
(14, 12, '_edit_last', '1'),
(15, 12, '_edit_lock', '1490275748:1'),
(17, 13, 'locked_image', ''),
(18, 13, '_locked_image', 'field_58cfd96a0cf5e'),
(19, 13, 'channel_for_unlocking', 'facebook'),
(20, 13, '_channel_for_unlocking', 'field_58cfda0978bfe'),
(21, 12, 'locked_image', '18'),
(22, 12, '_locked_image', 'field_58cfd96a0cf5e'),
(23, 12, 'channel_for_unlocking', 'facebook'),
(24, 12, '_channel_for_unlocking', 'field_58cfda0978bfe'),
(25, 17, '_wp_attached_file', '2017/03/yourfacelightsup-art.png'),
(26, 17, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:720;s:6:"height";i:480;s:4:"file";s:32:"2017/03/yourfacelightsup-art.png";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:32:"yourfacelightsup-art-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:32:"yourfacelightsup-art-300x200.png";s:5:"width";i:300;s:6:"height";i:200;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(27, 18, '_wp_attached_file', '2017/03/art-thumb-sample-pixelated.png'),
(28, 18, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:415;s:6:"height";i:247;s:4:"file";s:38:"2017/03/art-thumb-sample-pixelated.png";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:38:"art-thumb-sample-pixelated-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:38:"art-thumb-sample-pixelated-300x179.png";s:5:"width";i:300;s:6:"height";i:179;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(29, 12, '_thumbnail_id', '17'),
(31, 19, 'title', 'Your Face Lights Up'),
(32, 19, '_title', 'field_58cfdaadded87'),
(33, 19, 'locked_image', '18'),
(34, 19, '_locked_image', 'field_58cfd96a0cf5e'),
(35, 19, 'channel_for_unlocking', 'facebook'),
(36, 19, '_channel_for_unlocking', 'field_58cfda0978bfe'),
(37, 19, 'unlocks_at', '300'),
(38, 19, '_unlocks_at', 'field_58cfdad1ded88'),
(39, 19, 'current_number', '275'),
(40, 19, '_current_number', 'field_58cfdad8ded89'),
(41, 12, 'title', 'Your Face Lights Up'),
(42, 12, '_title', 'field_58cfdaadded87'),
(43, 12, 'unlocks_at', '300'),
(44, 12, '_unlocks_at', 'field_58cfdad1ded88'),
(45, 12, 'current_number', '275'),
(46, 12, '_current_number', 'field_58cfdad8ded89'),
(47, 21, '_wp_attached_file', '2017/03/art-thumb-sample.png'),
(48, 21, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:415;s:6:"height";i:247;s:4:"file";s:28:"2017/03/art-thumb-sample.png";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:28:"art-thumb-sample-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:28:"art-thumb-sample-300x179.png";s:5:"width";i:300;s:6:"height";i:179;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(50, 22, 'title', 'Your Face Lights Up'),
(51, 22, '_title', 'field_58cfdaadded87'),
(52, 22, 'locked_image', '18'),
(53, 22, '_locked_image', 'field_58cfd96a0cf5e'),
(54, 22, 'unlocked_image', '21'),
(55, 22, '_unlocked_image', 'field_58cfdb7322743'),
(56, 22, 'channel_for_unlocking', 'facebook'),
(57, 22, '_channel_for_unlocking', 'field_58cfda0978bfe'),
(58, 22, 'unlocks_at', '300'),
(59, 22, '_unlocks_at', 'field_58cfdad1ded88'),
(60, 22, 'current_number', '275'),
(61, 22, '_current_number', 'field_58cfdad8ded89'),
(62, 12, 'unlocked_image', '21'),
(63, 12, '_unlocked_image', 'field_58cfdb7322743'),
(65, 24, 'title', 'Your Face Lights Up'),
(66, 24, '_title', 'field_58cfdaadded87'),
(67, 24, 'locked_image', '18'),
(68, 24, '_locked_image', 'field_58cfd96a0cf5e'),
(69, 24, 'unlocked_image', '21'),
(70, 24, '_unlocked_image', 'field_58cfdb7322743'),
(71, 24, 'channel_for_unlocking', 'facebook'),
(72, 24, '_channel_for_unlocking', 'field_58cfda0978bfe'),
(73, 24, 'unlocks_at', '300'),
(74, 24, '_unlocks_at', 'field_58cfdad1ded88'),
(75, 24, 'current_number', '275'),
(76, 24, '_current_number', 'field_58cfdad8ded89'),
(77, 25, '_edit_last', '1'),
(78, 25, '_thumbnail_id', '17'),
(80, 26, 'unlocked', '0'),
(81, 26, '_unlocked', 'field_58cfdbd2cd3ec'),
(82, 26, 'title', 'Take It Easy'),
(83, 26, '_title', 'field_58cfdaadded87'),
(84, 26, 'locked_image', '18'),
(85, 26, '_locked_image', 'field_58cfd96a0cf5e'),
(86, 26, 'unlocked_image', '21'),
(87, 26, '_unlocked_image', 'field_58cfdb7322743'),
(88, 26, 'channel_for_unlocking', 'twitter'),
(89, 26, '_channel_for_unlocking', 'field_58cfda0978bfe'),
(90, 26, 'unlocks_at', '300'),
(91, 26, '_unlocks_at', 'field_58cfdad1ded88'),
(92, 26, 'current_number', '275'),
(93, 26, '_current_number', 'field_58cfdad8ded89'),
(94, 25, 'unlocked', '0'),
(95, 25, '_unlocked', 'field_58cfdbd2cd3ec'),
(96, 25, 'title', 'Take It Easy'),
(97, 25, '_title', 'field_58cfdaadded87'),
(98, 25, 'locked_image', '18'),
(99, 25, '_locked_image', 'field_58cfd96a0cf5e'),
(100, 25, 'unlocked_image', '21'),
(101, 25, '_unlocked_image', 'field_58cfdb7322743'),
(102, 25, 'channel_for_unlocking', 'twitter'),
(103, 25, '_channel_for_unlocking', 'field_58cfda0978bfe'),
(104, 25, 'unlocks_at', '300'),
(105, 25, '_unlocks_at', 'field_58cfdad1ded88') ;
INSERT INTO `blingbling_postmeta` ( `meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(106, 25, 'current_number', '275'),
(107, 25, '_current_number', 'field_58cfdad8ded89'),
(108, 25, '_edit_lock', '1490055040:1'),
(110, 12, 'unlocked', '1'),
(111, 12, '_unlocked', 'field_58cfdbd2cd3ec'),
(113, 12, 'chapter_title', 'Your Face Lights Up'),
(114, 12, '_chapter_title', 'field_58cfdaadded87'),
(116, 28, 'unlocked', '1'),
(117, 28, '_unlocked', 'field_58cfdbd2cd3ec'),
(118, 28, 'chapter_title', 'Your Face Lights Up'),
(119, 28, '_chapter_title', 'field_58cfdaadded87'),
(120, 28, 'locked_image', '18'),
(121, 28, '_locked_image', 'field_58cfd96a0cf5e'),
(122, 28, 'unlocked_image', '21'),
(123, 28, '_unlocked_image', 'field_58cfdb7322743'),
(124, 28, 'channel_for_unlocking', 'facebook'),
(125, 28, '_channel_for_unlocking', 'field_58cfda0978bfe'),
(126, 28, 'unlocks_at', '300'),
(127, 28, '_unlocks_at', 'field_58cfdad1ded88'),
(128, 28, 'current_number', '275'),
(129, 28, '_current_number', 'field_58cfdad8ded89'),
(130, 29, '_edit_last', '1'),
(131, 29, '_edit_lock', '1490275773:1'),
(132, 33, '_thumbnail_id', '17'),
(133, 33, 'unlocked', '0'),
(134, 33, '_unlocked', 'field_58cfdbd2cd3ec'),
(135, 33, 'title', 'Take It Easy'),
(136, 33, '_title', 'field_58cfdaadded87'),
(137, 33, 'locked_image', '18'),
(138, 33, '_locked_image', 'field_58cfd96a0cf5e'),
(139, 33, 'unlocked_image', '21'),
(140, 33, '_unlocked_image', 'field_58cfdb7322743'),
(141, 33, 'channel_for_unlocking', 'facebook'),
(142, 33, '_channel_for_unlocking', 'field_58cfda0978bfe'),
(143, 33, 'unlocks_at', '300'),
(144, 33, '_unlocks_at', 'field_58cfdad1ded88'),
(145, 33, 'current_number', '50'),
(146, 33, '_current_number', 'field_58cfdad8ded89'),
(147, 33, '_dp_original', '25'),
(148, 33, '_edit_last', '1'),
(149, 33, '_edit_lock', '1490275754:1'),
(151, 35, '_thumbnail_id', '17'),
(152, 35, 'unlocked', '0'),
(153, 35, '_unlocked', 'field_58cfdbd2cd3ec'),
(154, 35, 'title', 'Take It Easy'),
(155, 35, '_title', 'field_58cfdaadded87'),
(156, 35, 'locked_image', '18'),
(157, 35, '_locked_image', 'field_58cfd96a0cf5e'),
(158, 35, 'unlocked_image', '21'),
(159, 35, '_unlocked_image', 'field_58cfdb7322743'),
(160, 35, 'channel_for_unlocking', 'spotify'),
(161, 35, '_channel_for_unlocking', 'field_58cfda0978bfe'),
(162, 35, 'unlocks_at', '300'),
(163, 35, '_unlocks_at', 'field_58cfdad1ded88'),
(164, 35, 'current_number', '275'),
(165, 35, '_current_number', 'field_58cfdad8ded89'),
(167, 35, '_dp_original', '33'),
(168, 36, '_thumbnail_id', '17'),
(169, 36, 'unlocked', '0'),
(170, 36, '_unlocked', 'field_58cfdbd2cd3ec'),
(171, 36, 'title', 'Take It Easy'),
(172, 36, '_title', 'field_58cfdaadded87'),
(173, 36, 'locked_image', '18'),
(174, 36, '_locked_image', 'field_58cfd96a0cf5e'),
(175, 36, 'unlocked_image', '21'),
(176, 36, '_unlocked_image', 'field_58cfdb7322743'),
(177, 36, 'channel_for_unlocking', 'instagram'),
(178, 36, '_channel_for_unlocking', 'field_58cfda0978bfe'),
(179, 36, 'unlocks_at', '300'),
(180, 36, '_unlocks_at', 'field_58cfdad1ded88'),
(181, 36, 'current_number', '275'),
(182, 36, '_current_number', 'field_58cfdad8ded89'),
(184, 36, '_dp_original', '35'),
(185, 37, '_thumbnail_id', '17'),
(186, 37, 'unlocked', '1'),
(187, 37, '_unlocked', 'field_58cfdbd2cd3ec'),
(188, 37, 'title', 'Take It Easy'),
(189, 37, '_title', 'field_58cfdaadded87'),
(190, 37, 'locked_image', '18'),
(191, 37, '_locked_image', 'field_58cfd96a0cf5e'),
(192, 37, 'unlocked_image', '21'),
(193, 37, '_unlocked_image', 'field_58cfdb7322743'),
(194, 37, 'channel_for_unlocking', 'twitter'),
(195, 37, '_channel_for_unlocking', 'field_58cfda0978bfe'),
(196, 37, 'unlocks_at', '300'),
(197, 37, '_unlocks_at', 'field_58cfdad1ded88'),
(198, 37, 'current_number', '275'),
(199, 37, '_current_number', 'field_58cfdad8ded89'),
(201, 37, '_dp_original', '36'),
(202, 35, '_edit_last', '1'),
(204, 35, '_edit_lock', '1490305273:1'),
(205, 36, '_edit_last', '1'),
(207, 36, '_edit_lock', '1490304875:1'),
(208, 37, '_edit_lock', '1490058697:1'),
(209, 37, '_edit_last', '1'),
(211, 40, 'unlocked', '0'),
(212, 40, '_unlocked', 'field_58cfdbd2cd3ec'),
(213, 40, 'chapter_title', ''),
(214, 40, '_chapter_title', 'field_58cfdaadded87'),
(215, 40, 'locked_image', '18') ;
INSERT INTO `blingbling_postmeta` ( `meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(216, 40, '_locked_image', 'field_58cfd96a0cf5e'),
(217, 40, 'unlocked_image', '21'),
(218, 40, '_unlocked_image', 'field_58cfdb7322743'),
(219, 40, 'channel_for_unlocking', 'twitter'),
(220, 40, '_channel_for_unlocking', 'field_58cfda0978bfe'),
(221, 40, 'unlocks_at', '300'),
(222, 40, '_unlocks_at', 'field_58cfdad1ded88'),
(223, 40, 'current_number', '275'),
(224, 40, '_current_number', 'field_58cfdad8ded89'),
(225, 37, 'chapter_title', ''),
(226, 37, '_chapter_title', 'field_58cfdaadded87'),
(228, 41, 'unlocked', '0'),
(229, 41, '_unlocked', 'field_58cfdbd2cd3ec'),
(230, 41, 'chapter_title', ''),
(231, 41, '_chapter_title', 'field_58cfdaadded87'),
(232, 41, 'locked_image', '18'),
(233, 41, '_locked_image', 'field_58cfd96a0cf5e'),
(234, 41, 'unlocked_image', '21'),
(235, 41, '_unlocked_image', 'field_58cfdb7322743'),
(236, 41, 'channel_for_unlocking', 'twitter'),
(237, 41, '_channel_for_unlocking', 'field_58cfda0978bfe'),
(238, 41, 'unlocks_at', '300'),
(239, 41, '_unlocks_at', 'field_58cfdad1ded88'),
(240, 41, 'current_number', '275'),
(241, 41, '_current_number', 'field_58cfdad8ded89'),
(242, 36, 'chapter_title', ''),
(243, 36, '_chapter_title', 'field_58cfdaadded87'),
(244, 43, '_wp_attached_file', '2017/03/main-art.png'),
(245, 43, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:831;s:6:"height";i:865;s:4:"file";s:20:"2017/03/main-art.png";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:20:"main-art-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:20:"main-art-288x300.png";s:5:"width";i:288;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}s:12:"medium_large";a:4:{s:4:"file";s:20:"main-art-768x799.png";s:5:"width";i:768;s:6:"height";i:799;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(247, 44, 'unlocked', '0'),
(248, 44, '_unlocked', 'field_58cfdbd2cd3ec'),
(249, 44, 'chapter_title', ''),
(250, 44, '_chapter_title', 'field_58cfdaadded87'),
(251, 44, 'locked_image', '18'),
(252, 44, '_locked_image', 'field_58cfd96a0cf5e'),
(253, 44, 'unlocked_image', '21'),
(254, 44, '_unlocked_image', 'field_58cfdb7322743'),
(255, 44, 'channel_for_unlocking', 'facebook'),
(256, 44, '_channel_for_unlocking', 'field_58cfda0978bfe'),
(257, 44, 'unlocks_at', '300'),
(258, 44, '_unlocks_at', 'field_58cfdad1ded88'),
(259, 44, 'current_number', '50'),
(260, 44, '_current_number', 'field_58cfdad8ded89'),
(261, 33, 'chapter_title', ''),
(262, 33, '_chapter_title', 'field_58cfdaadded87'),
(264, 45, 'unlocked', '1'),
(265, 45, '_unlocked', 'field_58cfdbd2cd3ec'),
(266, 45, 'chapter_title', ''),
(267, 45, '_chapter_title', 'field_58cfdaadded87'),
(268, 45, 'locked_image', '18'),
(269, 45, '_locked_image', 'field_58cfd96a0cf5e'),
(270, 45, 'unlocked_image', '21'),
(271, 45, '_unlocked_image', 'field_58cfdb7322743'),
(272, 45, 'channel_for_unlocking', 'twitter'),
(273, 45, '_channel_for_unlocking', 'field_58cfda0978bfe'),
(274, 45, 'unlocks_at', '300'),
(275, 45, '_unlocks_at', 'field_58cfdad1ded88'),
(276, 45, 'current_number', '275'),
(277, 45, '_current_number', 'field_58cfdad8ded89'),
(279, 49, 'unlocked', '1'),
(280, 49, '_unlocked', 'field_58cfdbd2cd3ec'),
(281, 49, 'chapter_title', 'Your Face Lights Up'),
(282, 49, '_chapter_title', 'field_58cfdaadded87'),
(283, 49, 'locked_image', '18'),
(284, 49, '_locked_image', 'field_58cfd96a0cf5e'),
(285, 49, 'unlocked_image', '21'),
(286, 49, '_unlocked_image', 'field_58cfdb7322743'),
(287, 49, 'channel_for_unlocking', 'facebook'),
(288, 49, '_channel_for_unlocking', 'field_58cfda0978bfe'),
(289, 49, 'unlocks_at', '300'),
(290, 49, '_unlocks_at', 'field_58cfdad1ded88'),
(291, 49, 'current_number', '275'),
(292, 49, '_current_number', 'field_58cfdad8ded89'),
(293, 49, 'voice_over_link', 'http://localhost:5757/wp-content/themes/ferrycorsten/sounds/rave_digger.mp3'),
(294, 49, '_voice_over_link', 'field_58d3cc8182c35'),
(295, 49, 'preview_link', 'http://localhost:5757/wp-content/themes/ferrycorsten/sounds/running_out.mp3'),
(296, 49, '_preview_link', 'field_58d3cc9382c36'),
(297, 12, 'voice_over_link', 'http://localhost:5757/wp-content/themes/ferrycorsten/sounds/rave_digger.mp3'),
(298, 12, '_voice_over_link', 'field_58d3cc8182c35'),
(299, 12, 'preview_link', 'http://localhost:5757/wp-content/themes/ferrycorsten/sounds/running_out.mp3'),
(300, 12, '_preview_link', 'field_58d3cc9382c36'),
(302, 51, 'unlocked', '0'),
(303, 51, '_unlocked', 'field_58cfdbd2cd3ec'),
(304, 51, 'chapter_title', ''),
(305, 51, '_chapter_title', 'field_58cfdaadded87'),
(306, 51, 'locked_image', '18'),
(307, 51, '_locked_image', 'field_58cfd96a0cf5e'),
(308, 51, 'unlocked_image', '21'),
(309, 51, '_unlocked_image', 'field_58cfdb7322743'),
(310, 51, 'channel_for_unlocking', 'spotify: Spotify'),
(311, 51, '_channel_for_unlocking', 'field_58cfda0978bfe'),
(312, 51, 'unlocks_at', '300'),
(313, 51, '_unlocks_at', 'field_58cfdad1ded88'),
(314, 51, 'current_number', '275'),
(315, 51, '_current_number', 'field_58cfdad8ded89'),
(316, 51, 'voice_over_link', ''),
(317, 51, '_voice_over_link', 'field_58d3cc8182c35'),
(318, 51, 'preview_link', ''),
(319, 51, '_preview_link', 'field_58d3cc9382c36'),
(320, 35, 'chapter_title', '') ;
INSERT INTO `blingbling_postmeta` ( `meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(321, 35, '_chapter_title', 'field_58cfdaadded87'),
(322, 35, 'voice_over_link', ''),
(323, 35, '_voice_over_link', 'field_58d3cc8182c35'),
(324, 35, 'preview_link', ''),
(325, 35, '_preview_link', 'field_58d3cc9382c36'),
(327, 52, 'unlocked', '0'),
(328, 52, '_unlocked', 'field_58cfdbd2cd3ec'),
(329, 52, 'chapter_title', ''),
(330, 52, '_chapter_title', 'field_58cfdaadded87'),
(331, 52, 'locked_image', '18'),
(332, 52, '_locked_image', 'field_58cfd96a0cf5e'),
(333, 52, 'unlocked_image', '21'),
(334, 52, '_unlocked_image', 'field_58cfdb7322743'),
(335, 52, 'channel_for_unlocking', 'spotify'),
(336, 52, '_channel_for_unlocking', 'field_58cfda0978bfe'),
(337, 52, 'unlocks_at', '300'),
(338, 52, '_unlocks_at', 'field_58cfdad1ded88'),
(339, 52, 'current_number', '275'),
(340, 52, '_current_number', 'field_58cfdad8ded89'),
(341, 52, 'voice_over_link', ''),
(342, 52, '_voice_over_link', 'field_58d3cc8182c35'),
(343, 52, 'preview_link', ''),
(344, 52, '_preview_link', 'field_58d3cc9382c36'),
(346, 53, 'unlocked', '0'),
(347, 53, '_unlocked', 'field_58cfdbd2cd3ec'),
(348, 53, 'chapter_title', ''),
(349, 53, '_chapter_title', 'field_58cfdaadded87'),
(350, 53, 'locked_image', '18'),
(351, 53, '_locked_image', 'field_58cfd96a0cf5e'),
(352, 53, 'unlocked_image', '21'),
(353, 53, '_unlocked_image', 'field_58cfdb7322743'),
(354, 53, 'channel_for_unlocking', 'instagram'),
(355, 53, '_channel_for_unlocking', 'field_58cfda0978bfe'),
(356, 53, 'unlocks_at', '300'),
(357, 53, '_unlocks_at', 'field_58cfdad1ded88'),
(358, 53, 'current_number', '275'),
(359, 53, '_current_number', 'field_58cfdad8ded89'),
(360, 53, 'voice_over_link', ''),
(361, 53, '_voice_over_link', 'field_58d3cc8182c35'),
(362, 53, 'preview_link', ''),
(363, 53, '_preview_link', 'field_58d3cc9382c36'),
(364, 36, 'voice_over_link', ''),
(365, 36, '_voice_over_link', 'field_58d3cc8182c35'),
(366, 36, 'preview_link', ''),
(367, 36, '_preview_link', 'field_58d3cc9382c36'),
(369, 55, 'unlocked', '0'),
(370, 55, '_unlocked', 'field_58cfdbd2cd3ec'),
(371, 55, 'chapter_title', ''),
(372, 55, '_chapter_title', 'field_58cfdaadded87'),
(373, 55, 'locked_image', '18'),
(374, 55, '_locked_image', 'field_58cfd96a0cf5e'),
(375, 55, 'unlocked_image', '21'),
(376, 55, '_unlocked_image', 'field_58cfdb7322743'),
(377, 55, 'channel_for_unlocking', 'instagram'),
(378, 55, '_channel_for_unlocking', 'field_58cfda0978bfe'),
(379, 55, 'hashtags', '#ferrycorsten'),
(380, 55, '_hashtags', 'field_58d3d6307508b'),
(381, 55, 'unlocks_at', '300'),
(382, 55, '_unlocks_at', 'field_58cfdad1ded88'),
(383, 55, 'current_number', '275'),
(384, 55, '_current_number', 'field_58cfdad8ded89'),
(385, 55, 'voice_over_link', ''),
(386, 55, '_voice_over_link', 'field_58d3cc8182c35'),
(387, 55, 'preview_link', ''),
(388, 55, '_preview_link', 'field_58d3cc9382c36'),
(389, 36, 'hashtags', '#ferrycorsten'),
(390, 36, '_hashtags', 'field_58d3d6307508b') ;

#
# End of data contents of table `blingbling_postmeta`
# --------------------------------------------------------



#
# Delete any existing table `blingbling_posts`
#

DROP TABLE IF EXISTS `blingbling_posts`;


#
# Table structure of table `blingbling_posts`
#

CREATE TABLE `blingbling_posts` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_author` bigint(20) unsigned NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `post_name` (`post_name`(191)),
  KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  KEY `post_parent` (`post_parent`),
  KEY `post_author` (`post_author`)
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


#
# Data contents of table `blingbling_posts`
#
INSERT INTO `blingbling_posts` ( `ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(1, 1, '2017-03-13 15:36:33', '2017-03-13 15:36:33', 'Welcome to WordPress. This is your first post. Edit or delete it, then start writing!', 'Hello world!', '', 'trash', 'open', 'open', '', 'hello-world__trashed', '', '', '2017-03-20 13:31:08', '2017-03-20 13:31:08', '', 0, 'http://localhost:8888/?p=1', 0, 'post', '', 1),
(2, 1, '2017-03-13 15:36:33', '2017-03-13 15:36:33', 'This is an example page. It\'s different from a blog post because it will stay in one place and will show up in your site navigation (in most themes). Most people start with an About page that introduces them to potential site visitors. It might say something like this:\n\n<blockquote>Hi there! I\'m a bike messenger by day, aspiring actor by night, and this is my website. I live in Los Angeles, have a great dog named Jack, and I like pi&#241;a coladas. (And gettin\' caught in the rain.)</blockquote>\n\n...or something like this:\n\n<blockquote>The XYZ Doohickey Company was founded in 1971, and has been providing quality doohickeys to the public ever since. Located in Gotham City, XYZ employs over 2,000 people and does all kinds of awesome things for the Gotham community.</blockquote>\n\nAs a new WordPress user, you should go to <a href="http://localhost:8888/wp-admin/">your dashboard</a> to delete this page and create new pages for your content. Have fun!', 'Sample Page', '', 'trash', 'closed', 'open', '', 'sample-page__trashed', '', '', '2017-03-13 15:40:17', '2017-03-13 15:40:17', '', 0, 'http://localhost:8888/?page_id=2', 0, 'page', '', 0),
(4, 1, '2017-03-13 15:40:17', '2017-03-13 15:40:17', 'This is an example page. It\'s different from a blog post because it will stay in one place and will show up in your site navigation (in most themes). Most people start with an About page that introduces them to potential site visitors. It might say something like this:\n\n<blockquote>Hi there! I\'m a bike messenger by day, aspiring actor by night, and this is my website. I live in Los Angeles, have a great dog named Jack, and I like pi&#241;a coladas. (And gettin\' caught in the rain.)</blockquote>\n\n...or something like this:\n\n<blockquote>The XYZ Doohickey Company was founded in 1971, and has been providing quality doohickeys to the public ever since. Located in Gotham City, XYZ employs over 2,000 people and does all kinds of awesome things for the Gotham community.</blockquote>\n\nAs a new WordPress user, you should go to <a href="http://localhost:8888/wp-admin/">your dashboard</a> to delete this page and create new pages for your content. Have fun!', 'Sample Page', '', 'inherit', 'closed', 'closed', '', '2-revision-v1', '', '', '2017-03-13 15:40:17', '2017-03-13 15:40:17', '', 2, 'http://localhost:8888/2017/03/13/2-revision-v1/', 0, 'revision', '', 0),
(5, 1, '2017-03-13 15:40:48', '2017-03-13 15:40:48', '', 'Home', '', 'publish', 'closed', 'closed', '', 'home', '', '', '2017-03-13 15:44:31', '2017-03-13 15:44:31', '', 0, 'http://localhost:8888/?page_id=5', 0, 'page', '', 0),
(6, 1, '2017-03-13 15:40:48', '2017-03-13 15:40:48', '', 'Home', '', 'inherit', 'closed', 'closed', '', '5-revision-v1', '', '', '2017-03-13 15:40:48', '2017-03-13 15:40:48', '', 5, 'http://localhost:8888/2017/03/13/5-revision-v1/', 0, 'revision', '', 0),
(7, 1, '2017-03-20 13:31:01', '2017-03-20 13:31:01', 'a:7:{s:8:"location";a:1:{i:0;a:1:{i:0;a:3:{s:5:"param";s:9:"post_type";s:8:"operator";s:2:"==";s:5:"value";s:4:"post";}}}s:8:"position";s:6:"normal";s:5:"style";s:7:"default";s:15:"label_placement";s:3:"top";s:21:"instruction_placement";s:5:"label";s:14:"hide_on_screen";s:0:"";s:11:"description";s:0:"";}', 'Chapters', 'chapters', 'publish', 'closed', 'closed', '', 'group_58cfd967cdb57', '', '', '2017-03-23 14:06:06', '2017-03-23 14:06:06', '', 0, 'http://localhost:8888/?post_type=acf-field-group&#038;p=7', 0, 'acf-field-group', '', 0),
(8, 1, '2017-03-20 13:31:01', '2017-03-20 13:31:01', 'a:15:{s:4:"type";s:5:"image";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"return_format";s:5:"array";s:12:"preview_size";s:6:"medium";s:7:"library";s:3:"all";s:9:"min_width";s:0:"";s:10:"min_height";s:0:"";s:8:"min_size";s:0:"";s:9:"max_width";s:0:"";s:10:"max_height";s:0:"";s:8:"max_size";s:0:"";s:10:"mime_types";s:0:"";}', 'Locked Image', 'locked_image', 'publish', 'closed', 'closed', '', 'field_58cfd96a0cf5e', '', '', '2017-03-20 13:40:56', '2017-03-20 13:40:56', '', 7, 'http://localhost:8888/?post_type=acf-field&#038;p=8', 2, 'acf-field', '', 0),
(9, 1, '2017-03-20 13:31:08', '2017-03-20 13:31:08', 'Welcome to WordPress. This is your first post. Edit or delete it, then start writing!', 'Hello world!', '', 'inherit', 'closed', 'closed', '', '1-revision-v1', '', '', '2017-03-20 13:31:08', '2017-03-20 13:31:08', '', 1, 'http://localhost:8888/2017/03/20/1-revision-v1/', 0, 'revision', '', 0),
(10, 1, '2017-03-20 13:31:12', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2017-03-20 13:31:12', '0000-00-00 00:00:00', '', 0, 'http://localhost:8888/?p=10', 0, 'post', '', 0),
(11, 1, '2017-03-20 13:33:43', '2017-03-20 13:33:43', 'a:14:{s:4:"type";s:6:"select";s:12:"instructions";s:0:"";s:8:"required";i:1;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:7:"choices";a:4:{s:8:"facebook";s:8:"Facebook";s:7:"twitter";s:7:"Twitter";s:7:"spotify";s:7:"Spotify";s:9:"instagram";s:9:"Instagram";}s:13:"default_value";a:0:{}s:10:"allow_null";i:0;s:8:"multiple";i:0;s:2:"ui";i:0;s:4:"ajax";i:0;s:11:"placeholder";s:0:"";s:8:"disabled";i:0;s:8:"readonly";i:0;}', 'Channel for Unlocking', 'channel_for_unlocking', 'publish', 'closed', 'closed', '', 'field_58cfda0978bfe', '', '', '2017-03-23 13:32:13', '2017-03-23 13:32:13', '', 7, 'http://localhost:8888/?post_type=acf-field&#038;p=11', 4, 'acf-field', '', 0),
(12, 1, '2017-03-20 13:36:34', '2017-03-20 13:36:34', '<div class="wrapper">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos molestiae, et, voluptatem obcaecati laboriosam dolor nulla deserunt inventore, corporis ab tempora ipsum adipisci ut soluta, iste reprehenderit provident nesciunt sapiente.  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos molestiae, et, voluptatem obcaecati laboriosam dolor nulla deserunt inventore, corporis ab tempora ipsum adipisci ut soluta, iste reprehenderit provident nesciunt sapiente.  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos molestiae, et, voluptatem obcaecati laboriosam dolor nulla deserunt inventore, corporis ab tempora ipsum adipisci ut soluta, iste reprehenderit provident nesciunt sapiente.</div>\r\n<div class="wrapper">\r\n<div class="wrapper">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos molestiae, et, voluptatem obcaecati laboriosam dolor nulla deserunt inventore, corporis ab tempora ipsum adipisci ut soluta, iste reprehenderit provident nesciunt sapiente.  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos molestiae, et, voluptatem obcaecati laboriosam dolor nulla deserunt inventore, corporis ab tempora ipsum adipisci ut soluta, iste reprehenderit provident nesciunt sapiente.  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos molestiae, et, voluptatem obcaecati laboriosam dolor nulla deserunt inventore, corporis ab tempora ipsum adipisci ut soluta, iste reprehenderit provident nesciunt sapiente.</div>\r\n<div class="reveal-overlay"></div>\r\n</div>\r\n<div class="reveal-overlay"></div>', '01', '', 'publish', 'closed', 'closed', '', '01', '', '', '2017-03-23 13:27:13', '2017-03-23 13:27:13', '', 0, 'http://localhost:8888/?p=12', 0, 'post', '', 0),
(13, 1, '2017-03-20 13:36:34', '2017-03-20 13:36:34', '<div class="wrapper"></div>\r\n<div class="reveal-overlay">\r\n<div id="modal01" class="reveal fullscreen-modal mui-leave mui-leave-active slide mui-enter mui-enter-active" tabindex="-1" data-animation-in="slide" data-animation-out="slide" data-reveal="psp806-reveal" data-yeti-box="modal01" data-resize="modal01" data-events="resize">\r\n<div class="row padding-bottom content-margin-xtra-large"></div>\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Eos molestiae, et, voluptatem obcaecati laboriosam dolor nulla deserunt inventore, corporis ab tempora ipsum adipisci ut soluta, iste reprehenderit provident nesciunt sapiente.\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Eos molestiae, et, voluptatem obcaecati laboriosam dolor nulla deserunt inventore, corporis ab tempora ipsum adipisci ut soluta, iste reprehenderit provident nesciunt sapiente.\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Eos molestiae, et, voluptatem obcaecati laboriosam dolor nulla deserunt inventore, corporis ab tempora ipsum adipisci ut soluta, iste reprehenderit provident nesciunt sapiente.\r\n<div id="single-content" class="row"></div>\r\n</div>\r\n</div>', '01', '', 'inherit', 'closed', 'closed', '', '12-revision-v1', '', '', '2017-03-20 13:36:34', '2017-03-20 13:36:34', '', 12, 'http://localhost:8888/2017/03/20/12-revision-v1/', 0, 'revision', '', 0),
(14, 1, '2017-03-20 13:36:40', '2017-03-20 13:36:40', 'a:12:{s:4:"type";s:4:"text";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";s:7:"prepend";s:0:"";s:6:"append";s:0:"";s:9:"maxlength";s:0:"";s:8:"readonly";i:0;s:8:"disabled";i:0;}', 'Chapter Title', 'chapter_title', 'publish', 'closed', 'closed', '', 'field_58cfdaadded87', '', '', '2017-03-20 14:07:29', '2017-03-20 14:07:29', '', 7, 'http://localhost:8888/?post_type=acf-field&#038;p=14', 1, 'acf-field', '', 0),
(15, 1, '2017-03-20 13:36:40', '2017-03-20 13:36:40', 'a:12:{s:4:"type";s:4:"text";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";s:7:"prepend";s:0:"";s:6:"append";s:0:"";s:9:"maxlength";s:0:"";s:8:"readonly";i:0;s:8:"disabled";i:0;}', 'Unlocks At', 'unlocks_at', 'publish', 'closed', 'closed', '', 'field_58cfdad1ded88', '', '', '2017-03-23 14:05:57', '2017-03-23 14:05:57', '', 7, 'http://localhost:8888/?post_type=acf-field&#038;p=15', 6, 'acf-field', '', 0),
(16, 1, '2017-03-20 13:36:40', '2017-03-20 13:36:40', 'a:12:{s:4:"type";s:4:"text";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";s:7:"prepend";s:0:"";s:6:"append";s:0:"";s:9:"maxlength";s:0:"";s:8:"readonly";i:0;s:8:"disabled";i:0;}', 'Current Number', 'current_number', 'publish', 'closed', 'closed', '', 'field_58cfdad8ded89', '', '', '2017-03-23 14:05:57', '2017-03-23 14:05:57', '', 7, 'http://localhost:8888/?post_type=acf-field&#038;p=16', 7, 'acf-field', '', 0),
(17, 1, '2017-03-20 13:37:40', '2017-03-20 13:37:40', '', 'yourfacelightsup-art', '', 'inherit', 'closed', 'closed', '', 'yourfacelightsup-art', '', '', '2017-03-20 13:37:40', '2017-03-20 13:37:40', '', 12, 'http://localhost:8888/wp-content/uploads/2017/03/yourfacelightsup-art.png', 0, 'attachment', 'image/png', 0),
(18, 1, '2017-03-20 13:38:24', '2017-03-20 13:38:24', '', 'art-thumb-sample-pixelated', '', 'inherit', 'closed', 'closed', '', 'art-thumb-sample-pixelated', '', '', '2017-03-20 13:38:24', '2017-03-20 13:38:24', '', 12, 'http://localhost:8888/wp-content/uploads/2017/03/art-thumb-sample-pixelated.png', 0, 'attachment', 'image/png', 0),
(19, 1, '2017-03-20 13:38:53', '2017-03-20 13:38:53', '<div class="wrapper"></div>\r\n<div class="reveal-overlay">\r\n<div id="modal01" class="reveal fullscreen-modal mui-leave mui-leave-active slide mui-enter mui-enter-active" tabindex="-1" data-animation-in="slide" data-animation-out="slide" data-reveal="psp806-reveal" data-yeti-box="modal01" data-resize="modal01" data-events="resize">\r\n<div class="row padding-bottom content-margin-xtra-large"></div>\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Eos molestiae, et, voluptatem obcaecati laboriosam dolor nulla deserunt inventore, corporis ab tempora ipsum adipisci ut soluta, iste reprehenderit provident nesciunt sapiente.\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Eos molestiae, et, voluptatem obcaecati laboriosam dolor nulla deserunt inventore, corporis ab tempora ipsum adipisci ut soluta, iste reprehenderit provident nesciunt sapiente.\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Eos molestiae, et, voluptatem obcaecati laboriosam dolor nulla deserunt inventore, corporis ab tempora ipsum adipisci ut soluta, iste reprehenderit provident nesciunt sapiente.\r\n<div id="single-content" class="row"></div>\r\n</div>\r\n</div>', '01', '', 'inherit', 'closed', 'closed', '', '12-revision-v1', '', '', '2017-03-20 13:38:53', '2017-03-20 13:38:53', '', 12, 'http://localhost:8888/2017/03/20/12-revision-v1/', 0, 'revision', '', 0),
(20, 1, '2017-03-20 13:39:14', '2017-03-20 13:39:14', 'a:15:{s:4:"type";s:5:"image";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"return_format";s:5:"array";s:12:"preview_size";s:6:"medium";s:7:"library";s:3:"all";s:9:"min_width";s:0:"";s:10:"min_height";s:0:"";s:8:"min_size";s:0:"";s:9:"max_width";s:0:"";s:10:"max_height";s:0:"";s:8:"max_size";s:0:"";s:10:"mime_types";s:0:"";}', 'Unlocked Image', 'unlocked_image', 'publish', 'closed', 'closed', '', 'field_58cfdb7322743', '', '', '2017-03-20 13:40:56', '2017-03-20 13:40:56', '', 7, 'http://localhost:8888/?post_type=acf-field&#038;p=20', 3, 'acf-field', '', 0),
(21, 1, '2017-03-20 13:39:28', '2017-03-20 13:39:28', '', 'art-thumb-sample', '', 'inherit', 'closed', 'closed', '', 'art-thumb-sample', '', '', '2017-03-20 13:39:28', '2017-03-20 13:39:28', '', 12, 'http://localhost:8888/wp-content/uploads/2017/03/art-thumb-sample.png', 0, 'attachment', 'image/png', 0),
(22, 1, '2017-03-20 13:39:32', '2017-03-20 13:39:32', '<div class="wrapper"></div>\r\n<div class="reveal-overlay">\r\n<div id="modal01" class="reveal fullscreen-modal mui-leave mui-leave-active slide mui-enter mui-enter-active" tabindex="-1" data-animation-in="slide" data-animation-out="slide" data-reveal="psp806-reveal" data-yeti-box="modal01" data-resize="modal01" data-events="resize">\r\n<div class="row padding-bottom content-margin-xtra-large"></div>\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Eos molestiae, et, voluptatem obcaecati laboriosam dolor nulla deserunt inventore, corporis ab tempora ipsum adipisci ut soluta, iste reprehenderit provident nesciunt sapiente.\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Eos molestiae, et, voluptatem obcaecati laboriosam dolor nulla deserunt inventore, corporis ab tempora ipsum adipisci ut soluta, iste reprehenderit provident nesciunt sapiente.\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Eos molestiae, et, voluptatem obcaecati laboriosam dolor nulla deserunt inventore, corporis ab tempora ipsum adipisci ut soluta, iste reprehenderit provident nesciunt sapiente.\r\n<div id="single-content" class="row"></div>\r\n</div>\r\n</div>', '01', '', 'inherit', 'closed', 'closed', '', '12-revision-v1', '', '', '2017-03-20 13:39:32', '2017-03-20 13:39:32', '', 12, 'http://localhost:8888/2017/03/20/12-revision-v1/', 0, 'revision', '', 0),
(23, 1, '2017-03-20 13:40:56', '2017-03-20 13:40:56', 'a:7:{s:4:"type";s:10:"true_false";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:7:"message";s:0:"";s:13:"default_value";i:1;}', 'Unlocked?', 'unlocked', 'publish', 'closed', 'closed', '', 'field_58cfdbd2cd3ec', '', '', '2017-03-20 13:42:55', '2017-03-20 13:42:55', '', 7, 'http://localhost:8888/?post_type=acf-field&#038;p=23', 0, 'acf-field', '', 0),
(24, 1, '2017-03-20 13:41:08', '2017-03-20 13:41:08', '<div class="wrapper"> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos molestiae, et, voluptatem obcaecati laboriosam dolor nulla deserunt inventore, corporis ab tempora ipsum adipisci ut soluta, iste reprehenderit provident nesciunt sapiente.</div>\r\n<div class="reveal-overlay">\r\n<div id="modal01" class="reveal fullscreen-modal mui-leave mui-leave-active slide mui-enter mui-enter-active" tabindex="-1" data-animation-in="slide" data-animation-out="slide" data-reveal="psp806-reveal" data-yeti-box="modal01" data-resize="modal01" data-events="resize">\r\n<div class="row padding-bottom content-margin-xtra-large">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos molestiae, et, voluptatem obcaecati laboriosam dolor nulla deserunt inventore, corporis ab tempora ipsum adipisci ut soluta, iste reprehenderit provident nesciunt sapiente.\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Eos molestiae, et, voluptatem obcaecati laboriosam dolor nulla deserunt inventore, corporis ab tempora ipsum adipisci ut soluta, iste reprehenderit provident nesciunt sapiente.</div>\r\n<div id="single-content" class="row"></div>\r\n</div>\r\n</div>', '01', '', 'inherit', 'closed', 'closed', '', '12-revision-v1', '', '', '2017-03-20 13:41:08', '2017-03-20 13:41:08', '', 12, 'http://localhost:8888/2017/03/20/12-revision-v1/', 0, 'revision', '', 0),
(25, 1, '2017-03-20 13:46:34', '2017-03-20 13:46:34', '', '02', '', 'publish', 'closed', 'closed', '', '02', '', '', '2017-03-20 13:46:34', '2017-03-20 13:46:34', '', 0, 'http://localhost:8888/?p=25', 0, 'post', '', 0),
(26, 1, '2017-03-20 13:46:34', '2017-03-20 13:46:34', '', '02', '', 'inherit', 'closed', 'closed', '', '25-revision-v1', '', '', '2017-03-20 13:46:34', '2017-03-20 13:46:34', '', 25, 'http://localhost:8888/2017/03/20/25-revision-v1/', 0, 'revision', '', 0),
(27, 1, '2017-03-20 14:09:41', '2017-03-20 14:09:41', '<div class="wrapper"> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos molestiae, et, voluptatem obcaecati laboriosam dolor nulla deserunt inventore, corporis ab tempora ipsum adipisci ut soluta, iste reprehenderit provident nesciunt sapiente.  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos molestiae, et, voluptatem obcaecati laboriosam dolor nulla deserunt inventore, corporis ab tempora ipsum adipisci ut soluta, iste reprehenderit provident nesciunt sapiente.  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos molestiae, et, voluptatem obcaecati laboriosam dolor nulla deserunt inventore, corporis ab tempora ipsum adipisci ut soluta, iste reprehenderit provident nesciunt sapiente.</div>\n<div class="wrapper">\n<div class="wrapper">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos molestiae, et, voluptatem obcaecati laboriosam dolor nulla deserunt inventore, corporis ab tempora ipsum adipisci ut soluta, iste reprehenderit provident nesciunt sapiente.  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos molestiae, et, voluptatem obcaecati laboriosam dolor nulla deserunt inventore, corporis ab tempora ipsum adipisci ut soluta, iste reprehenderit provident nesciunt sapiente.  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos molestiae, et, voluptatem obcaecati laboriosam dolor nulla deserunt inventore, corporis ab tempora ipsum adipisci ut soluta, iste reprehenderit provident nesciunt sapiente.</div>\n<div class="reveal-overlay"></div>\n</div>\n<div class="reveal-overlay">\n<div id="modal01" class="reveal fullscreen-modal mui-leave mui-leave-active slide mui-enter mui-enter-active" tabindex="-1" data-animation-in="slide" data-animation-out="slide" data-reveal="psp806-reveal" data-yeti-box="modal01" data-resize="modal01" data-events="resize">\n<div id="single-content" class="row"></div>\n</div>\n</div>', '01', '', 'inherit', 'closed', 'closed', '', '12-autosave-v1', '', '', '2017-03-20 14:09:41', '2017-03-20 14:09:41', '', 12, 'http://localhost:8888/2017/03/20/12-autosave-v1/', 0, 'revision', '', 0),
(28, 1, '2017-03-20 14:10:17', '2017-03-20 14:10:17', '<div class="wrapper">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos molestiae, et, voluptatem obcaecati laboriosam dolor nulla deserunt inventore, corporis ab tempora ipsum adipisci ut soluta, iste reprehenderit provident nesciunt sapiente.  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos molestiae, et, voluptatem obcaecati laboriosam dolor nulla deserunt inventore, corporis ab tempora ipsum adipisci ut soluta, iste reprehenderit provident nesciunt sapiente.  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos molestiae, et, voluptatem obcaecati laboriosam dolor nulla deserunt inventore, corporis ab tempora ipsum adipisci ut soluta, iste reprehenderit provident nesciunt sapiente.</div>\r\n<div class="wrapper">\r\n<div class="wrapper">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos molestiae, et, voluptatem obcaecati laboriosam dolor nulla deserunt inventore, corporis ab tempora ipsum adipisci ut soluta, iste reprehenderit provident nesciunt sapiente.  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos molestiae, et, voluptatem obcaecati laboriosam dolor nulla deserunt inventore, corporis ab tempora ipsum adipisci ut soluta, iste reprehenderit provident nesciunt sapiente.  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos molestiae, et, voluptatem obcaecati laboriosam dolor nulla deserunt inventore, corporis ab tempora ipsum adipisci ut soluta, iste reprehenderit provident nesciunt sapiente.</div>\r\n<div class="reveal-overlay"></div>\r\n</div>\r\n<div class="reveal-overlay">\r\n<div id="modal01" class="reveal fullscreen-modal mui-leave mui-leave-active slide mui-enter mui-enter-active" tabindex="-1" data-animation-in="slide" data-animation-out="slide" data-reveal="psp806-reveal" data-yeti-box="modal01" data-resize="modal01" data-events="resize">\r\n<div id="single-content" class="row"></div>\r\n</div>\r\n</div>', '01', '', 'inherit', 'closed', 'closed', '', '12-revision-v1', '', '', '2017-03-20 14:10:17', '2017-03-20 14:10:17', '', 12, 'http://localhost:8888/2017/03/20/12-revision-v1/', 0, 'revision', '', 0),
(29, 1, '2017-03-20 14:21:50', '2017-03-20 14:21:50', 'a:7:{s:8:"location";a:1:{i:0;a:1:{i:0;a:3:{s:5:"param";s:12:"options_page";s:8:"operator";s:2:"==";s:5:"value";s:16:"general-settings";}}}s:8:"position";s:6:"normal";s:5:"style";s:7:"default";s:15:"label_placement";s:3:"top";s:21:"instruction_placement";s:5:"label";s:14:"hide_on_screen";s:0:"";s:11:"description";s:0:"";}', 'Settings', 'settings', 'publish', 'closed', 'closed', '', 'group_58cfe507a31af', '', '', '2017-03-23 13:30:18', '2017-03-23 13:30:18', '', 0, 'http://localhost:8888/?post_type=acf-field-group&#038;p=29', 0, 'acf-field-group', '', 0),
(30, 1, '2017-03-20 14:21:50', '2017-03-20 14:21:50', 'a:10:{s:4:"type";s:8:"repeater";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:9:"collapsed";s:0:"";s:3:"min";s:0:"";s:3:"max";s:0:"";s:6:"layout";s:5:"table";s:12:"button_label";s:7:"Add Row";}', 'Socials', 'socials', 'publish', 'closed', 'closed', '', 'field_58cfe52265a81', '', '', '2017-03-20 14:21:50', '2017-03-20 14:21:50', '', 29, 'http://localhost:8888/?post_type=acf-field&p=30', 0, 'acf-field', '', 0),
(31, 1, '2017-03-20 14:21:50', '2017-03-20 14:21:50', 'a:14:{s:4:"type";s:6:"select";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:7:"choices";a:3:{s:8:"facebook";s:8:"Facebook";s:9:"instagram";s:9:"Instagram";s:7:"twitter";s:7:"Twitter";}s:13:"default_value";a:0:{}s:10:"allow_null";i:0;s:8:"multiple";i:0;s:2:"ui";i:0;s:4:"ajax";i:0;s:11:"placeholder";s:0:"";s:8:"disabled";i:0;s:8:"readonly";i:0;}', 'Network', 'network', 'publish', 'closed', 'closed', '', 'field_58cfe54565a84', '', '', '2017-03-20 14:21:50', '2017-03-20 14:21:50', '', 30, 'http://localhost:8888/?post_type=acf-field&p=31', 0, 'acf-field', '', 0),
(32, 1, '2017-03-20 14:21:50', '2017-03-20 14:21:50', 'a:12:{s:4:"type";s:4:"text";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";s:7:"prepend";s:0:"";s:6:"append";s:0:"";s:9:"maxlength";s:0:"";s:8:"readonly";i:0;s:8:"disabled";i:0;}', 'Link', 'link', 'publish', 'closed', 'closed', '', 'field_58cfe56765a85', '', '', '2017-03-20 14:21:50', '2017-03-20 14:21:50', '', 30, 'http://localhost:8888/?post_type=acf-field&p=32', 1, 'acf-field', '', 0),
(33, 1, '2017-03-20 14:32:44', '2017-03-20 14:32:44', '', '03', '', 'publish', 'closed', 'closed', '', '03', '', '', '2017-03-21 00:16:21', '2017-03-21 00:16:21', '', 0, 'http://localhost:8888/?p=33', 0, 'post', '', 0),
(34, 1, '2017-03-20 14:32:51', '2017-03-20 14:32:51', '', '03', '', 'inherit', 'closed', 'closed', '', '33-revision-v1', '', '', '2017-03-20 14:32:51', '2017-03-20 14:32:51', '', 33, 'http://localhost:8888/2017/03/20/33-revision-v1/', 0, 'revision', '', 0),
(35, 1, '2017-03-20 14:33:07', '2017-03-20 14:33:07', '', '04', '', 'publish', 'closed', 'closed', '', '04', '', '', '2017-03-23 13:36:00', '2017-03-23 13:36:00', '', 0, 'http://localhost:8888/?p=35', 0, 'post', '', 0),
(36, 1, '2017-03-20 14:33:10', '2017-03-20 14:33:10', '', '05', '', 'publish', 'closed', 'closed', '', '04-2', '', '', '2017-03-23 21:33:14', '2017-03-23 21:33:14', '', 0, 'http://localhost:8888/?p=36', 0, 'post', '', 0),
(37, 1, '2017-03-20 14:33:33', '2017-03-20 14:33:33', '', '06', '', 'publish', 'closed', 'closed', '', '06', '', '', '2017-03-21 01:05:13', '2017-03-21 01:05:13', '', 0, 'http://localhost:8888/?p=37', 0, 'post', '', 0),
(38, 1, '2017-03-20 14:33:19', '2017-03-20 14:33:19', '', '04', '', 'inherit', 'closed', 'closed', '', '35-revision-v1', '', '', '2017-03-20 14:33:19', '2017-03-20 14:33:19', '', 35, 'http://localhost:8888/2017/03/20/35-revision-v1/', 0, 'revision', '', 0),
(39, 1, '2017-03-20 14:33:25', '2017-03-20 14:33:25', '', '04', '', 'inherit', 'closed', 'closed', '', '36-revision-v1', '', '', '2017-03-20 14:33:25', '2017-03-20 14:33:25', '', 36, 'http://localhost:8888/2017/03/20/36-revision-v1/', 0, 'revision', '', 0),
(40, 1, '2017-03-20 14:33:33', '2017-03-20 14:33:33', '', '06', '', 'inherit', 'closed', 'closed', '', '37-revision-v1', '', '', '2017-03-20 14:33:33', '2017-03-20 14:33:33', '', 37, 'http://localhost:8888/2017/03/20/37-revision-v1/', 0, 'revision', '', 0),
(41, 1, '2017-03-20 14:33:43', '2017-03-20 14:33:43', '', '05', '', 'inherit', 'closed', 'closed', '', '36-revision-v1', '', '', '2017-03-20 14:33:43', '2017-03-20 14:33:43', '', 36, 'http://localhost:8888/2017/03/20/36-revision-v1/', 0, 'revision', '', 0),
(42, 1, '2017-03-20 14:34:29', '2017-03-20 14:34:29', 'a:15:{s:4:"type";s:5:"image";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"return_format";s:5:"array";s:12:"preview_size";s:9:"thumbnail";s:7:"library";s:3:"all";s:9:"min_width";s:0:"";s:10:"min_height";s:0:"";s:8:"min_size";s:0:"";s:9:"max_width";s:0:"";s:10:"max_height";s:0:"";s:8:"max_size";s:0:"";s:10:"mime_types";s:0:"";}', 'Header Image', 'header_image', 'publish', 'closed', 'closed', '', 'field_58cfe86da407d', '', '', '2017-03-20 14:34:38', '2017-03-20 14:34:38', '', 29, 'http://localhost:8888/?post_type=acf-field&#038;p=42', 1, 'acf-field', '', 0),
(43, 1, '2017-03-20 14:35:03', '2017-03-20 14:35:03', '', 'main-art', '', 'inherit', 'closed', 'closed', '', 'main-art', '', '', '2017-03-20 14:35:03', '2017-03-20 14:35:03', '', 0, 'http://localhost:8888/wp-content/uploads/2017/03/main-art.png', 0, 'attachment', 'image/png', 0),
(44, 1, '2017-03-21 00:16:21', '2017-03-21 00:16:21', '', '03', '', 'inherit', 'closed', 'closed', '', '33-revision-v1', '', '', '2017-03-21 00:16:21', '2017-03-21 00:16:21', '', 33, 'http://localhost:8888/33-revision-v1/', 0, 'revision', '', 0),
(45, 1, '2017-03-21 01:05:13', '2017-03-21 01:05:13', '', '06', '', 'inherit', 'closed', 'closed', '', '37-revision-v1', '', '', '2017-03-21 01:05:13', '2017-03-21 01:05:13', '', 37, 'http://localhost:8888/37-revision-v1/', 0, 'revision', '', 0),
(46, 1, '2017-03-23 13:24:00', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2017-03-23 13:24:00', '0000-00-00 00:00:00', '', 0, 'http://localhost:8888/?p=46', 0, 'post', '', 0),
(47, 1, '2017-03-23 13:24:41', '2017-03-23 13:24:41', 'a:12:{s:4:"type";s:4:"text";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";s:7:"prepend";s:0:"";s:6:"append";s:0:"";s:9:"maxlength";s:0:"";s:8:"readonly";i:0;s:8:"disabled";i:0;}', 'Voice Over Link', 'voice_over_link', 'publish', 'closed', 'closed', '', 'field_58d3cc8182c35', '', '', '2017-03-23 14:05:57', '2017-03-23 14:05:57', '', 7, 'http://localhost:8888/?post_type=acf-field&#038;p=47', 8, 'acf-field', '', 0),
(48, 1, '2017-03-23 13:24:41', '2017-03-23 13:24:41', 'a:12:{s:4:"type";s:4:"text";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";s:7:"prepend";s:0:"";s:6:"append";s:0:"";s:9:"maxlength";s:0:"";s:8:"readonly";i:0;s:8:"disabled";i:0;}', 'Preview Link', 'preview_link', 'publish', 'closed', 'closed', '', 'field_58d3cc9382c36', '', '', '2017-03-23 14:05:57', '2017-03-23 14:05:57', '', 7, 'http://localhost:8888/?post_type=acf-field&#038;p=48', 9, 'acf-field', '', 0),
(49, 1, '2017-03-23 13:27:13', '2017-03-23 13:27:13', '<div class="wrapper">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos molestiae, et, voluptatem obcaecati laboriosam dolor nulla deserunt inventore, corporis ab tempora ipsum adipisci ut soluta, iste reprehenderit provident nesciunt sapiente.  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos molestiae, et, voluptatem obcaecati laboriosam dolor nulla deserunt inventore, corporis ab tempora ipsum adipisci ut soluta, iste reprehenderit provident nesciunt sapiente.  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos molestiae, et, voluptatem obcaecati laboriosam dolor nulla deserunt inventore, corporis ab tempora ipsum adipisci ut soluta, iste reprehenderit provident nesciunt sapiente.</div>\r\n<div class="wrapper">\r\n<div class="wrapper">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos molestiae, et, voluptatem obcaecati laboriosam dolor nulla deserunt inventore, corporis ab tempora ipsum adipisci ut soluta, iste reprehenderit provident nesciunt sapiente.  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos molestiae, et, voluptatem obcaecati laboriosam dolor nulla deserunt inventore, corporis ab tempora ipsum adipisci ut soluta, iste reprehenderit provident nesciunt sapiente.  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos molestiae, et, voluptatem obcaecati laboriosam dolor nulla deserunt inventore, corporis ab tempora ipsum adipisci ut soluta, iste reprehenderit provident nesciunt sapiente.</div>\r\n<div class="reveal-overlay"></div>\r\n</div>\r\n<div class="reveal-overlay"></div>', '01', '', 'inherit', 'closed', 'closed', '', '12-revision-v1', '', '', '2017-03-23 13:27:13', '2017-03-23 13:27:13', '', 12, 'http://localhost:8888/12-revision-v1/', 0, 'revision', '', 0),
(50, 1, '2017-03-23 13:30:18', '2017-03-23 13:30:18', 'a:12:{s:4:"type";s:4:"text";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";s:7:"prepend";s:0:"";s:6:"append";s:0:"";s:9:"maxlength";s:0:"";s:8:"readonly";i:0;s:8:"disabled";i:0;}', 'Spotify Follow URI', 'spotify_follow_uri', 'publish', 'closed', 'closed', '', 'field_58d3cde1d4df3', '', '', '2017-03-23 13:30:18', '2017-03-23 13:30:18', '', 29, 'http://localhost:8888/?post_type=acf-field&p=50', 2, 'acf-field', '', 0),
(51, 1, '2017-03-23 13:31:55', '2017-03-23 13:31:55', '', '04', '', 'inherit', 'closed', 'closed', '', '35-revision-v1', '', '', '2017-03-23 13:31:55', '2017-03-23 13:31:55', '', 35, 'http://localhost:8888/35-revision-v1/', 0, 'revision', '', 0),
(52, 1, '2017-03-23 13:36:00', '2017-03-23 13:36:00', '', '04', '', 'inherit', 'closed', 'closed', '', '35-revision-v1', '', '', '2017-03-23 13:36:00', '2017-03-23 13:36:00', '', 35, 'http://localhost:8888/35-revision-v1/', 0, 'revision', '', 0),
(53, 1, '2017-03-23 13:40:28', '2017-03-23 13:40:28', '', '05', '', 'inherit', 'closed', 'closed', '', '36-revision-v1', '', '', '2017-03-23 13:40:28', '2017-03-23 13:40:28', '', 36, 'http://localhost:8888/36-revision-v1/', 0, 'revision', '', 0),
(54, 1, '2017-03-23 14:05:57', '2017-03-23 14:05:57', 'a:12:{s:4:"type";s:4:"text";s:12:"instructions";s:0:"";s:8:"required";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";s:7:"prepend";s:0:"";s:6:"append";s:0:"";s:9:"maxlength";s:0:"";s:8:"readonly";i:0;s:8:"disabled";i:0;s:17:"conditional_logic";a:1:{i:0;a:1:{i:0;a:3:{s:5:"field";s:19:"field_58cfda0978bfe";s:8:"operator";s:2:"==";s:5:"value";s:9:"instagram";}}}}', 'Hashtags', 'hashtags', 'publish', 'closed', 'closed', '', 'field_58d3d6307508b', '', '', '2017-03-23 14:05:57', '2017-03-23 14:05:57', '', 7, 'http://localhost:8888/?post_type=acf-field&p=54', 5, 'acf-field', '', 0),
(55, 1, '2017-03-23 21:33:14', '2017-03-23 21:33:14', '', '05', '', 'inherit', 'closed', 'closed', '', '36-revision-v1', '', '', '2017-03-23 21:33:14', '2017-03-23 21:33:14', '', 36, 'http://localhost:8888/36-revision-v1/', 0, 'revision', '', 0) ;

#
# End of data contents of table `blingbling_posts`
# --------------------------------------------------------



#
# Delete any existing table `blingbling_term_relationships`
#

DROP TABLE IF EXISTS `blingbling_term_relationships`;


#
# Table structure of table `blingbling_term_relationships`
#

CREATE TABLE `blingbling_term_relationships` (
  `object_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  KEY `term_taxonomy_id` (`term_taxonomy_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


#
# Data contents of table `blingbling_term_relationships`
#
INSERT INTO `blingbling_term_relationships` ( `object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1, 1, 0),
(12, 1, 0),
(25, 1, 0),
(33, 1, 0),
(35, 1, 0),
(36, 1, 0),
(37, 1, 0) ;

#
# End of data contents of table `blingbling_term_relationships`
# --------------------------------------------------------



#
# Delete any existing table `blingbling_term_taxonomy`
#

DROP TABLE IF EXISTS `blingbling_term_taxonomy`;


#
# Table structure of table `blingbling_term_taxonomy`
#

CREATE TABLE `blingbling_term_taxonomy` (
  `term_taxonomy_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_taxonomy_id`),
  UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  KEY `taxonomy` (`taxonomy`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


#
# Data contents of table `blingbling_term_taxonomy`
#
INSERT INTO `blingbling_term_taxonomy` ( `term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 6) ;

#
# End of data contents of table `blingbling_term_taxonomy`
# --------------------------------------------------------



#
# Delete any existing table `blingbling_termmeta`
#

DROP TABLE IF EXISTS `blingbling_termmeta`;


#
# Table structure of table `blingbling_termmeta`
#

CREATE TABLE `blingbling_termmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `term_id` (`term_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


#
# Data contents of table `blingbling_termmeta`
#

#
# End of data contents of table `blingbling_termmeta`
# --------------------------------------------------------



#
# Delete any existing table `blingbling_terms`
#

DROP TABLE IF EXISTS `blingbling_terms`;


#
# Table structure of table `blingbling_terms`
#

CREATE TABLE `blingbling_terms` (
  `term_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_id`),
  KEY `slug` (`slug`(191)),
  KEY `name` (`name`(191))
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


#
# Data contents of table `blingbling_terms`
#
INSERT INTO `blingbling_terms` ( `term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Uncategorized', 'uncategorized', 0) ;

#
# End of data contents of table `blingbling_terms`
# --------------------------------------------------------



#
# Delete any existing table `blingbling_usermeta`
#

DROP TABLE IF EXISTS `blingbling_usermeta`;


#
# Table structure of table `blingbling_usermeta`
#

CREATE TABLE `blingbling_usermeta` (
  `umeta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`umeta_id`),
  KEY `user_id` (`user_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


#
# Data contents of table `blingbling_usermeta`
#
INSERT INTO `blingbling_usermeta` ( `umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'manufactur'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'comment_shortcuts', 'false'),
(7, 1, 'admin_color', 'fresh'),
(8, 1, 'use_ssl', '0'),
(9, 1, 'show_admin_bar_front', 'true'),
(10, 1, 'blingbling_capabilities', 'a:1:{s:13:"administrator";b:1;}'),
(11, 1, 'blingbling_user_level', '10'),
(12, 1, 'dismissed_wp_pointers', ''),
(13, 1, 'show_welcome_panel', '1'),
(14, 1, 'session_tokens', 'a:3:{s:64:"305ad9415d21279148719723faed461a600e3901cd15d2070f75fc976fc435e1";a:4:{s:10:"expiration";i:1490448239;s:2:"ip";s:9:"127.0.0.1";s:2:"ua";s:120:"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36";s:5:"login";i:1490275439;}s:64:"174acadeb4372746121d48be1c0aa117ed9eb732bb3dd96c4af3d338afeba2dc";a:4:{s:10:"expiration";i:1490468404;s:2:"ip";s:12:"108.60.37.22";s:2:"ua";s:120:"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36";s:5:"login";i:1490295604;}s:64:"25cb937254633e3d63dc72ae5695182272a2345dced78a49f928e5c91082ac17";a:4:{s:10:"expiration";i:1490471690;s:2:"ip";s:3:"::1";s:2:"ua";s:120:"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36";s:5:"login";i:1490298890;}}'),
(15, 1, 'blingbling_user-settings', 'libraryContent=browse&posts_list_mode=list&hidetb=1'),
(16, 1, 'blingbling_user-settings-time', '1489419547'),
(17, 1, 'blingbling_dashboard_quick_press_last_post_id', '46'),
(18, 1, 'meta-box-order_post', 'a:4:{s:15:"acf_after_title";s:0:"";s:4:"side";s:51:"postimagediv,submitdiv,categorydiv,tagsdiv-post_tag";s:6:"normal";s:88:"acf-group_58cfe507a31af,acf-group_58cfd967cdb57,postexcerpt,postcustom,slugdiv,authordiv";s:8:"advanced";s:0:"";}'),
(19, 1, 'screen_layout_post', '2'),
(20, 1, 'acf_user_settings', 'a:0:{}'),
(21, 1, 'meta-box-order_toplevel_page_general-settings', 'a:2:{s:4:"side";s:9:"submitdiv";s:6:"normal";s:23:"acf-group_58cfe507a31af";}'),
(22, 1, 'screen_layout_toplevel_page_general-settings', '2') ;

#
# End of data contents of table `blingbling_usermeta`
# --------------------------------------------------------



#
# Delete any existing table `blingbling_users`
#

DROP TABLE IF EXISTS `blingbling_users`;


#
# Table structure of table `blingbling_users`
#

CREATE TABLE `blingbling_users` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`),
  KEY `user_login_key` (`user_login`),
  KEY `user_nicename` (`user_nicename`),
  KEY `user_email` (`user_email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


#
# Data contents of table `blingbling_users`
#
INSERT INTO `blingbling_users` ( `ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'manufactur', '$P$BD/n88QpsGh9aPi8bcCrcgKsEnv1oA1', 'manufactur', 'nicholas@manufactur.co', '', '2017-03-13 15:36:32', '', 0, 'manufactur') ;

#
# End of data contents of table `blingbling_users`
# --------------------------------------------------------

#
# Add constraints back in and apply any alter data queries.
#

