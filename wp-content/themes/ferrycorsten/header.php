<!DOCTYPE html>
<html dir="ltr" lang="en-US">

<!--
========================================================================================================
=============================== Site by Manufactur.co ==================================================
========================================================================================================
-->

<head>

<!-- Basic Page Needs
================================================== -->
<meta charset="utf-8">
<title><?php wp_title(); ?></title>
<link rel="shortcut icon" href="/wp-content/themes/ferrycorsten/images/favicon.ico">
<link rel="apple-touch-icon" href="/wp-content/themes/ferrycorsten/images/apple-touch-icon.png">
<link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_url') ?>" />
<meta http-equiv="content-type" content="<?php bloginfo('html_type') ?>; charset=<?php bloginfo('charset') ?>" />	
<link rel="alternate" type="application/rss+xml" href="<?php bloginfo('rss2_url') ?>" title="<?php printf( __( '%s latest posts', 'sandbox' ), wp_specialchars( get_bloginfo('name'), 1 ) ) ?>" />
<link rel="pingback" href="<?php bloginfo('pingback_url') ?>" />

<!-- Mobile Specific Metas
================================================== -->	
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="format-detection" content="telephone=yes" />


<!-- Wordpress Header
================================================== -->	
<?php wp_head(); // For plugins ?>


<!-- Style Sheets
================================================== -->	
<!--[if lt IE 9]>
	<link rel="stylesheet" type="text/css" href="/wp-content/themes/THEME/deploy/ie.css" />
<![endif]-->


<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
<!-- fonts -->
<link type="text/css" rel="stylesheet" href="//fast.fonts.net/cssapi/9a4c74f2-30b0-43a8-84ea-0f5f403d3aef.css"/>


<script type="text/javascript">

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-61197926-37']);
    _gaq.push(['_gat._forceSSL']);
    _gaq.push(['_trackPageview']);

    (function () {
        var ga = document.createElement('script');
        ga.type = 'text/javascript';
        ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(ga, s);
    })();



</script>







</head>


<body <?php if (function_exists('body_class')) body_class(); ?>>
<div id="wrapper">

<header>
    <div class="row padding-bottom content-margin">
    	<div class="small-4 small-centered medium-12 medium-uncentered text-center columns content-margin">
			<div id="logo"><img src="<?php echo get_bloginfo('template_directory'); ?>/images/ferry-logo.png" alt="Ferry Corsten" /></div>
    	</div>  
    	<div class="small-12 columns">
			<div id="site-name" class="text-center"><h1 class="glow">BLUEPRINT</h1></div>
    	</div>     
    	<div class="small-12 text-center columns">
	    <ul id="socials" class="header-social inline-list">
                <?php if ( get_field( 'socials', 'option' ) ) : ?>
                    <?php while( has_sub_field( 'socials', 'option' ) ) : ?>
                    <li><a target="_blank" href="<?php echo get_sub_field('link'); ?>"><i class="fa fa-<?php echo get_sub_field('network'); ?>"></i></a></li>
                    <?php endwhile; ?>
                <?php endif; ?>
            </ul>
    	</div>        
    </div>
</header>