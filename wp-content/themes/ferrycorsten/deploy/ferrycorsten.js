// Custom JS

jQuery(function(){
    var sound = {};

    jQuery(".sound" ).click(function(){
        var track   = jQuery( this ).attr( 'data-track' );
        var action  = jQuery( this ).attr( 'data-player');
        //console.log( track );
        
        if ( action == 'play' ){

            if (typeof sound.stop !== "undefined") { 
                console.log( 'stop' );
                sound.stop();
            }

            console.log( 'play ' + track );
            sound = new Howl({
                src : [ track ]
            });
            
            sound.play();

            //remove any pause buttons that exist
            jQuery( '.sound' ).each( function( index ) {
                if ( jQuery( this ).find('.fa-pause') ) {
                    console.log( 'found it.');
                    jQuery( this ).find('.fa-pause').removeClass();
                    jQuery( this ).find( 'i' ).addClass( 'fa fa-play' );
                    jQuery( this ).removeAttr( 'data-player' );
                    jQuery( this ).attr( 'data-player', 'play' );
                }
            });

            jQuery( this ).find('.fa-play' ).removeClass();
            jQuery( this ).find('i').addClass( 'fa fa-pause' );
            jQuery( this ).removeAttr( 'data-player' );
            jQuery( this ).attr( 'data-player', 'pause' );


        }
        else if ( action == 'pause' ){

            sound.pause();

            console.log( 'pausing ' + track );
            jQuery( this ).find('.fa-pause' ).removeClass();
            jQuery( this ).find( 'i' ).addClass( 'fa fa-play' );
            jQuery( this ).removeAttr( 'data-player' );
            jQuery( this ).attr( 'data-player', 'play' );
        }

    });



    jQuery(".column-block").hover(function() 
        {
            jQuery(this).find('.instructions progress').animate({width:"100%"});
        }, function() 
        {
            jQuery(this).find('.instructions progress').animate({width:"0%"});
        }
    );


   
    jQuery(document).foundation();

    /*
    // smooth scroll to anchors
    jQuery(function() {
	  jQuery('a[href*="#"]:not([href="#"])').click(function() {
	    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
	      var target = $(this.hash);
	      target = target.length ? target : jQuery('[name=' + this.hash.slice(1) +']');
	      if (target.length) {
	        jQuery('html, body').animate({
	          scrollTop: target.offset().top - 100
	        }, 1000);
	        return false;
	      }
	    }
	  });
	});

	jQuery('.off-canvas .menu li a').click(function(){
    	jQuery('.js-off-canvas-exit').trigger( "click" );

    })
    */














});

