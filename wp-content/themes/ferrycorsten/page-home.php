<?php 

/*

Template Name: Home Page

*/

?>


<?php get_header() ?>

<div class="row">
	<div class="small-12 text-center columns">
		<?php $main_art = get_field('header_image', 'option'); ?>
		<img src="<?php echo $main_art['sizes']['large']; ?>" alt="Ferry Corsten" />
	</div>
</div>



<div class="row">
	<div class="small-12 text-center columns content-margin">
		<a href="<?php echo get_field('buy_link', 'options'); ?>" target="_blank"><h2><?php echo get_field('buy_link_text', 'options'); ?></h2></a>
	</div>
</div>



<?php query_posts( 'order=ASC'); ?>



<div class="row small-up-1 text-center medium-up-3 content-margin-large">
	<?php $i = 1; ?>
	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
		<?php $ID = get_the_ID(); ?>
		<?php if ( get_field( 'unlocked' ) == true ): ?>
			<div class="column column-block unlocked">
				<?php $image = get_field('unlocked_image'); ?>
				

				<div class="inner" style="background-image: url( <?php echo $image['sizes']['large']; ?> );">
					<span class="chapter-number glow"><?php the_title(); ?></span>
					<div class="content-container valign-container instructions">
						<div class="valign">
							<div class="action-link">
								<span class="link-wrapper"><a class="button glow" data-open="modal_<?php echo $i; ?>">Enter</a></span>
							</div>
						</div>
					</div>
				</div>
			</div>
	
		<?php else : ?>
			<?php $pixelimage = get_field('locked_image'); ?>
			<div class="column column-block locked">
				<div class="inner cover" style="background-image: url( <?php echo $pixelimage['sizes']['large']; ?> ); );">
					<div class="overlay full-absolute"></div>
					<span class="chapter-number glow"><?php the_title(); ?></span>
					<div class="content-container valign-container locked">
					    <div class="valign">
						    <span class="lock-icon"><i class="fa fa-lock"></i></span>
						    <span class="locked-text glow">Locked</span>
						</div>
				    </div>
					<div class="content-container valign-container instructions">
						<div class="valign">
							<div class="unlock-text content-margin-small glow">Unlocks At</div>
							<ul class="inline-list social-metric content-margin-small">
								<?php $fa_icon = 'fa-' . get_field( 'channel_for_unlocking'); ?>
								<li><i class="fa <?php echo $fa_icon; ?> glow"></i></li>
								<li class="social-number glow"><?php echo get_field('unlocks_at'); ?></li>
							</ul>
							<?php if ( get_field( 'channel_for_unlocking' ) == 'facebook' ) : ?>
								<!--<div class="action-link"><span class="link-wrapper"><a class="button glow" href="#">Share</a></span></div>-->
								<?php $buttons = 'Facebook'; ?>
								<?php echo do_shortcode( '[social_warfare post_id="5" buttons="' . $buttons . '"]'); ?>

							<?php elseif ( get_field( 'channel_for_unlocking' ) == 'twitter' ) : ?>
								<?php $buttons = 'Twitter'; ?>
								<?php echo do_shortcode( '[social_warfare post_id="5" buttons="' . $buttons . '"]'); ?>

							<?php elseif ( get_field( 'channel_for_unlocking' ) == 'spotify' ) : ?>
								<?php $spotify_uri = get_field('spotify_follow_uri', 'option'); ?>
								<iframe src="https://embed.spotify.com/follow/1/?uri=<?php echo $spotify_uri; ?>&size=basic&theme=light&show-count=0" width="100" height="25" scrolling="no" frameborder="0" class="spotify-button" style="border:none; overflow:hidden;" allowtransparency="true"></iframe>
								<!--<div class="action-link"><span class="link-wrapper"><a class="button glow" href="#">Tweet</a></span></div>-->

							<?php elseif ( get_field( 'channel_for_unlocking' ) == 'instagram' ) : ?>
								
								<div class="action-link instagram-desktop hide-for-large">
									<a class="button" href="instagram://user?username=ferrycorsten" target="_blank">Follow</a>
								</div>
								<div class="action-link instagram-desktop show-for-large">
									<a href="https://www.instagram.com/ferrycorsten/" target="_blank" class="button inactive title">Follow</a>
								</div>
								<!--
								<div class="reveal fullscreen-modal" data-animation-in="slide" data-animation-out="slide" id="insta_<?php echo $i; ?>" data-reveal>
								 	<button class="close-button" data-close aria-label="Close modal" type="button">
								    	<span aria-hidden="true">&times;</span>
								  	</button>
								  	<div class="full-absolute valign-container">
									 	<div class="valign">
									 		<div class="row padding-bottom content-margin-xtra-large">
												<div class="small-12 medium-10 large-8 small-centered text-center content-margin">
													<span class="text-center"><?php echo get_the_post_thumbnail( '', 'large'); ?></span>
												</div>
												<div class="small-12 medium-10 large-8 small-centered columns text-center content-margin">
													<div class="content-margin-large">
														<br><br><br><br>
														<h3 class="text-center">Follow Ferry on Instagram</h3>
													</div>
													<a class="button blue music-button" href="instagram://user?username=ferrycorsten">
														<h3>Open Instagram</h3>
													</a>
										 		</div>
											</div>
										</div>
									</div>
								</div>-->
							<?php elseif ( get_field( 'channel_for_unlocking' ) == 'envelope' ) : ?>
								<?php echo do_shortcode( "[gravityform id=1 title=false description=false ajax=true tabindex=49]" ); ?>

							<?php endif; ?>

							<?php $current_progress = get_field( 'current_number' ) / get_field( 'unlocks_at' ) * 100; ?>
							<progress class="show-for-large" max="100" value="<?php echo $current_progress; ?>" style="width:0%;"></progress>
							<progress class="hide-for-large" max="100" value="<?php echo $current_progress; ?>"></progress>
						</div>
					</div>
				</div>
			</div>
		<?php endif; ?>
		<?php ++$i; ?>
	<?php endwhile; endif; ?>
</div>


<?php $i = 1; ?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	<?php if ( get_field( 'unlocked' ) == true ): ?>
		<div class="reveal fullscreen-modal" data-animation-in="slide" data-animation-out="slide" id="modal_<?php echo $i; ?>" data-reveal>
		 	
		 	<div class="row padding-bottom content-margin-xl">
		 		<div class="small-12 columns text-center">
					<span class="large-number glow"><?php the_title(); ?></span>
		 		</div>
				<div class="small-12 columns">
			  		<div class="chapter-name"><h1 class="glow text-center"><?php echo get_field( 'chapter_title' ); ?></h1></div>
				</div> 

				<button class="close-button" data-close aria-label="Close modal" type="button">
			    	<span aria-hidden="true">&times;</span>
			  	</button>
			</div>
			
			<div class="row" id="single-content">
				
				<div class="small-12 medium-10 large-8 small-centered text-center content-margin">
					<span class="text-center">
						<?php echo get_the_post_thumbnail( '', 'large'); ?>
					</span>
				</div>
				
				<div class="small-12 medium-10 large-8 small-centered columns text-center content-margin">
					<a class="button blue music-button sound" data-player="play" data-track="<?php echo get_field('voice_over_link'); ?>">
						<i class="fa fa-play"></i>
						<h3><span>Voice</span><span>Over</span></h3>
					</a>
					<a class="button red music-button sound" data-player="play" data-track="<?php echo get_field('preview_link'); ?>">
						<i class="fa fa-play"></i>
						<h3><span>Pre</span><span>View</span></h3>
					</a>
				</div>
				


				<div class="small-12 medium-10 large-8 small-centered medium-8 columns content-margin">
					<h2>Chapter <?php the_title(); ?></h2>
					
					<div class="content-margin-large"><?php the_content(); ?></div>
			
				
					<div class="row">
						<div class="small-12 text-center columns content-margin">
							<a href="<?php echo get_field('buy_link', 'options'); ?>" target="_blank"><h2><?php echo get_field('buy_link_text', 'options'); ?></h2></a>
						</div>
					</div>


					<div class="row small-up-2 text-center medium-up-3 content-margin">
						<?php if ( get_field( 'merch_items' ) ): ?>
							<?php while ( has_sub_field( 'merch_items') ) : ?>
								<div class="column column-block">
									<?php $image = get_sub_field( 'image' ); ?>
									<div class="merch-inner">
										<a target="_blank" href="<?php echo get_sub_field('link'); ?>"><img src="<?php echo $image['sizes']['large']; ?>"></a>
										<!--<div class="content-container valign-container instructions">
											<div class="valign">
												<div class="action-link">
													<span class="link-wrapper"><a class="button glow" target="_blank" href="<?php echo get_sub_field('link'); ?>">Buy Now</a></span>
												</div>
											</div>
										</div>-->
									</div>
								</div>
							<?php endwhile; ?>
						<?php endif; ?>
					</div>


	
					<div class="content-margin-large text-center small-12 medium-4 columns small-centered">
						<h2>Share</h2>
						<?php echo do_shortcode( '[social_warfare post_id="5" buttons="Facebook, Twitter"]'); ?>
					</div>

					<div class="content-margin-large text-center">
						<div class="back-button" data-close aria-label="Close modal">Back</div>
					</div>


				</div>


			</div>
			
		</div>
			  
		  </div>
		</div>
	<?php endif; ?>
	<?php ++$i; ?>
<?php endwhile; endif; ?>

<?php get_footer() ?>