

		<footer class="text-center">
		    <p>© Ferry Corsten 2017<span>|</span><span><a href="http://manufactur.co" target="_blank">site by Manufactur</a></span></p>
		    
		</footer>


		<?php wp_footer() ?>


	</div><!--.wrapper-->



    <!-- Facebook Pixel Code -->
    <script>
    !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
    n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
    document,'script','https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '444255639104123'); // Insert your pixel ID here.
    fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=444255639104123&ev=PageView&noscript=1"
    /></noscript>
    <!-- DO NOT MODIFY -->
    <!-- End Facebook Pixel Code -->



	</body>




<script type="text/javascript">

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-61197926-37']);
    _gaq.push(['_gat._forceSSL']);
    _gaq.push(['_trackPageview']);

    (function () {
        var ga = document.createElement('script');
        ga.type = 'text/javascript';
        ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(ga, s);
    })();



jQuery(function(){
    //Track clicks on share links
    //facebook
    jQuery('.nc_tweetContainer.swp_fb a').click(function(){ 
        _gaq.push(['_trackEvent', 'Chapter Grid', 'Share Click', 'Facebook']);
        console.log('click');
    });
    //twitter
    jQuery('.nc_tweetContainer a').click(function(){ 
        _gaq.push(['_trackEvent', 'Chapter Grid', 'Share Click', 'Twitter']);
        console.log('click');
    });
    //instagram
    jQuery('.nc_tweetContainer.instagram a').click(function(){ 
        _gaq.push(['_trackEvent', 'Chapter Grid', 'Share Click', 'Instagram']);
        console.log('click');
    });
    jQuery('.spotify-button').click(function(){ 
        _gaq.push(['_trackEvent', 'Chapter Grid', 'Share Click', 'Spotify']);
        console.log('click');
    });
});



</script>





</html>
